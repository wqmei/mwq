var domain = "localhost"; // 域
var port = "8080"; // 端口

/* 测试alert，可一键关闭 */
function ALERT(msg) {
	//alert(msg);
}

/*
 * alert模态框 function alertModal(hd, bd) { $("#alert_hd").html(hd);
 * $("#alert_bd").html(bd); $("#alert_div").modal(); }
 */

/* alert模态框 */
function alertModal(hd, bd, closeFun) {
	$('body').append("<div style='z-index:9999' class='am-modal am-modal-alert' tabindex='-1' id='alert_div'>" +
		"<div class='am-modal-dialog'style='z-index=9999'>" +
			"<div class='am-modal-hd' id='alert_hd'></div>" +
			"<div class='am-modal-bd' id='alert_bd'></div>" +
			"<div class='am-modal-footer'>" +
				"<span data-am-modal-close class='am-modal-btn '>确定</span>" +
			"</div>" +
		"</div>" +
	"</div>");
	
	$("#alert_hd").html(hd);
	$("#alert_bd").html(bd);
	$("#alert_div").modal();

	// 监听alert模态框关闭后，执行closeFun
	// 这里必须使用one
	if (typeof (closeFun) != "undefined") {
		$('#alert_div').one("close.modal.amui", closeFun);
	}
}

/* json加密 */
function encrypt(content) {
	return JSON.stringify(content);
	// TODO 加密
}

/* json解密 */
function unencrypt(data) {
	return data;
}

/* 处理ajax请求发生错误 */
function handleAjaxError() {
	alertModal("警告", "服务器开小差了");
}

/* 设置cookie */
function setCookie(name, value) {
	/* 获取cookie工具 */
	var cookie = $.AMUI.utils.cookie;
	cookie.set(name, value, getCookieTime(), "/", domain);
}

/* 获得cookie */
function getCookie(name) {
	/* 获取cookie工具 */
	var cookie = $.AMUI.utils.cookie;
	var value = cookie.get(name);
	if (value == "undefined" || value == null || value == "null") {
		return null;
	} else {
		return value;
	}
}

/* 删除cookie */
function removeCookie(name) {
	/* 获取cookie工具 */
	var cookie = $.AMUI.utils.cookie;
	cookie.set(name, null, 0, "/", domain);
}

/* 获取cookie有效时间 */
function getCookieTime() {
	end = new Date();
	end = new Date(end.valueOf() + 7 * 24 * 60 * 60 * 1000); // 7天有效
	return end;
}

/* 从时间戳中获取时间 */
function getTime(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1)
			: datetime.getMonth() + 1;
	var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime
			.getDate();
	var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime
			.getHours();
	var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes()
			: datetime.getMinutes();
	var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds()
			: datetime.getSeconds();
	return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":"
			+ second;
}

/*从时间戳中获取时分秒*/
function getHMS(time){
	var datetime = new Date();
	datetime.setTime(time);
	var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime
			.getHours();
	var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes()
			: datetime.getMinutes();
	var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds()
			: datetime.getSeconds();
	return hour + ":" + minute + ":" + second;
}

/* 从时间戳中获取日期 */
function getDate(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1)
			: datetime.getMonth() + 1;
	var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime
			.getDate();
	return year + "-" + month + "-" + date;
}

/*从时间戳中获取不带日期的时间*/
function getTimeWithoutDate(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime
			.getHours();
	var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes()
			: datetime.getMinutes();
	return hour + ":" + minute;
}

/* 判断用户是否登录 */
function isLogin() {
	var pkUser = getCookie("pkUser");
	if (pkUser == null) {
		alertModal("提示", "您未登陆，请先登录", function() {
			window.location.href = "/view/mobile/login/login.html";
		});
		return false;
	}
	return true;
}

/* confirm模态框 */
function confirmModal(hd, bd, confirmFun, cancelFun) {
	$('body').append("<div class='am-modal am-modal-confirm' tabindex='-1' id='my-confirm'>" +
		"<div class='am-modal-dialog'>" +
			"<div id='confirm_hd' class='am-modal-hd'></div>" +
				"<div id='confirm_bd' class='am-modal-bd'></div>" +
			"<div class='am-modal-footer'>" +
				"<span data-am-modal-close class='am-modal-btn' data-am-modal-cancel>取消</span> <span data-am-modal-confirm class='am-modal-btn' data-am-modal-confirm>确定</span>" +
			"</div>" +
		"</div>" +
	"</div>");
	
	$("#confirm_hd").html(hd);
	$("#confirm_bd").html(bd);
	$("#my-confirm").modal({
		onConfirm : confirmFun,
		onCancel : cancelFun
	});
	
	$('#my-confirm').one("close.modal.amui", function() {
		$(this).remove();
	});
}

/* 根据一个整数，返回一个颜色 */
function getColor(num) {
	var colors = [ "red", "blue", "green", "purple", "brown"];

	return colors[num % colors.length];
}

/* 获取url参数 */
function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}

/*将时间字符串转为时间戳*/
function getTimestamp(timeStr) {
    var newstr = timeStr.replace(/-/g,'/'); 
    var date =  new Date(newstr); 
    var time_str = date.getTime().toString();
    return time_str;
}




