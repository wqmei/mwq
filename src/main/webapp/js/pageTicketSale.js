
/* 全部售票记录页面分页跳转 */
function PageBtn(){
	
	/* 分页数字跳转  */
	$(".start").on("click",function(e){
	var dataPage = $(this).attr("data-page");

		$.ajax({
		type : "GET",
		url : "/orders/curPage/"+dataPage+"/pageSize/10" ,
		contentType : "application/json",
		dataType : "json",
		success : function(data) {
		ALERT(JSON.stringify(data));
		if(data.code == "200") {
		var pag = data.pageInfo.totalCount; //信息列表总记录数目
		showTicketSale(data);
		setTicType();
		setOdrStatus();
		PageBtn();
		} else {
			ALERT(data.msg);
			alertModal("警告", data.msg);
			}
		},
			error : function() {
			handleAjaxError();
					}
			});   
	})
	
	/* 跳转按钮跳转 */
	$("#jumpBtn").on("click",function(e){

		var dataPage = $("#jumpIpt").val();

			$.ajax({
			type : "GET",
			url : "/orders/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showTicketSale(data);
			setTicType();
			setOdrStatus();
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
		});   
	})
	
	/* 下一页按钮跳转 */
	$("#nextContent").on("click",function(e){

		var curPage = parseInt($(".active").text());
		var dataPage = curPage +1 ; 
			$.ajax({
			type : "GET",
			url : "/orders/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showTicketSale(data);
			setTicType();
			setOdrStatus();
			pagination(pag,dataPage);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
			/* 上一页按钮跳转 */
	$("#prevContent").on("click",function(e){

		var curPage = parseInt($(".active").text());
		var dataPage = curPage -1 ; 
			$.ajax({
			type : "GET",
			url : "/orders/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showTicketSale(data);
			setTicType();
			setOdrStatus();
			pagination(pag,dataPage);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
				/* 首页按钮跳转 */
	$("#home").on("click",function(e){

			$.ajax({
			type : "GET",
			url : "/orders/curPage/1/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showTicketSale(data);
			setTicType();
			setOdrStatus();
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
			/* 末页按钮跳转 */
	$("#end").on("click",function(e){

			$.ajax({
			type : "GET",
			url :"/orders/curPage/" + totalPage + "/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showTicketSale(data);
			setTicType();
			setOdrStatus();
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			

 }
 

/*未付款页面分页跳转*/
function PageBtnNoPay(){
			$(".start").on("click",function(e){
			/* 分页数字跳转  */

			var dataPage = $(this).attr("data-page");

	  		var odrStatus = 1;
			var content = {};
			content["odrStatus"] = odrStatus; 
			content = encrypt(content);
			ALERT(content);
      
   		$.ajax({
			type : "GET",
			url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
			data : content,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
				ALERT(JSON.stringify(data));
				if(data.code == "200") {
				var pag = data.pageInfo.totalCount; //信息列表总记录数目
				showNoPay(data);
				setTicType();
				setOdrStatus();
				PageBtnNoPay();	
				} else {
					ALERT(data.msg);
					alertModal("警告", data.msg);
				}
			},
			error : function() {
				handleAjaxError();
			}
		}); 
		
			})
			
			/* 跳转按钮跳转 */
			$("#jumpBtn").on("click",function(e){

				var dataPage = $("#jumpIpt").val();
	  			var odrStatus = 1;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoPay(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoPay();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
				});   
			})
			
			/* 下一页按钮跳转 */
			 $("#nextContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage +1 ; 
	   			var odrStatus = 1;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				 $.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoPay(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoPay();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  
 
					})
					 
					/* 上一页按钮跳转 */
			$("#prevContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage -1 ; 
	   			var odrStatus = 1;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoPay(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoPay();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
				/* 首页按钮跳转 */
			$("#home").on("click",function(e){
 
	   			var odrStatus = 1;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/1/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoPay(data);
					setTicType();
					setOdrStatus();
					PageBtnNoPay();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
					/* 末页按钮跳转 */
			$("#end").on("click",function(e){
 
	   			var odrStatus = 1;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ totalPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoPay(data);
					setTicType();
					setOdrStatus();
					PageBtnNoPay();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})

         }


/*未取票页面分页跳转*/
function PageBtnNoCollect(){
			$(".start").on("click",function(e){
			/* 分页数字跳转  */

			var dataPage = $(this).attr("data-page");

	  		var ticStatus = 1;
			var content = {};
			content["ticStatus"] = ticStatus; 
			content = encrypt(content);
			ALERT(content);
      
   		$.ajax({
			type : "GET",
			url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
			data : content,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
				ALERT(JSON.stringify(data));
				if(data.code == "200") {
				var pag = data.pageInfo.totalCount; //信息列表总记录数目
				showNoCollect(data);
				setTicType();
				setOdrStatus();
				PageBtnNoCollect();	
				} else {
					ALERT(data.msg);
					alertModal("警告", data.msg);
				}
			},
			error : function() {
				handleAjaxError();
			}
		}); 
		
			})
			
			/* 跳转按钮跳转 */
			$("#jumpBtn").on("click",function(e){

				var dataPage = $("#jumpIpt").val();
	  			var ticStatus = 1;
				var content = {};
				content["ticStatus"] = ticStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoCollect(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoCollect();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
				});   
			})
			
			/* 下一页按钮跳转 */
			 $("#nextContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage +1 ; 
	   			var ticStatus = 1;
				var content = {};
				content["ticStatus"] = ticStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				 $.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoCollect(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoCollect();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  
 
					})
					 
					/* 上一页按钮跳转 */
			$("#prevContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage -1 ; 
	   			var ticStatus = 1;
				var content = {};
				content["ticStatus"] = ticStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoCollect(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnNoCollect();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
				/* 首页按钮跳转 */
			$("#home").on("click",function(e){
 
	   			var ticStatus = 1;
				var content = {};
				content["ticStatus"] = ticStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/1/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoCollect(data);
					setTicType();
					setOdrStatus();
					PageBtnNoCollect();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
					/* 末页按钮跳转 */
			$("#end").on("click",function(e){
 
	   			var ticStatus = 1;
				var content = {};
				content["ticStatus"] = ticStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ totalPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showNoCollect(data);
					setTicType();
					setOdrStatus();
					PageBtnNoCollect();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})

         }


 
/*已失效页面分页跳转*/
function PageBtnOdrValid(){
			$(".start").on("click",function(e){
			/* 分页数字跳转  */

			var dataPage = $(this).attr("data-page");

	  		var odrStatus = 4;
			var content = {};
			content["odrStatus"] = odrStatus; 
			content = encrypt(content);
			ALERT(content);
      
   		$.ajax({
			type : "GET",
			url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
			data : content,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
				ALERT(JSON.stringify(data));
				if(data.code == "200") {
				var pag = data.pageInfo.totalCount; //信息列表总记录数目
				showOdrValid(data);
				setTicType();
				setOdrStatus();
				PageBtnOdrValid();	
				} else {
					ALERT(data.msg);
					alertModal("警告", data.msg);
				}
			},
			error : function() {
				handleAjaxError();
			}
		}); 
		
			})
			
			/* 跳转按钮跳转 */
			$("#jumpBtn").on("click",function(e){

				var dataPage = $("#jumpIpt").val();
	  			var odrStatus = 4;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showOdrValid(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnOdrValid();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
				});   
			})
			
			/* 下一页按钮跳转 */
			 $("#nextContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage +1 ; 
	   			var odrStatus = 4;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				 $.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showOdrValid(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnOdrValid();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  
 
					})
					 
					/* 上一页按钮跳转 */
			$("#prevContent").on("click",function(e){

				var curPage = parseInt($(".active").text());
				var dataPage = curPage -1 ; 
	   			var odrStatus = 4;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ dataPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showOdrValid(data);
					setTicType();
					setOdrStatus();
					pagination(pag,dataPage);
					PageBtnOdrValid();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
				/* 首页按钮跳转 */
			$("#home").on("click",function(e){
 
	   			var odrStatus = 4;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/1/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showOdrValid(data);
					setTicType();
					setOdrStatus();
					PageBtnOdrValid();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})
					
					/* 末页按钮跳转 */
			$("#end").on("click",function(e){
 
	   			var odrStatus = 4;
				var content = {};
				content["odrStatus"] = odrStatus; 
				content = encrypt(content);
				ALERT(content);
      
   				$.ajax({
					type : "GET",
					url : "/orders/advance/curPage/"+ totalPage +"/pageSize/10" ,
					data : content,
					contentType : "application/json",
					dataType : "json",
					success : function(data) {
					ALERT(JSON.stringify(data));
					if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					showOdrValid(data);
					setTicType();
					setOdrStatus();
					PageBtnOdrValid();	
					} else {
						ALERT(data.msg);
						alertModal("警告", data.msg);
						}
					},
					error : function() {
					handleAjaxError();
						}
					});  

					})

         }