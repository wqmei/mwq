/*10.	查看党员详情*/

function viewPartyMem(node){
	var pkPm = node.getAttribute("pkPm");
	setCookie("pkPm",pkPm);
	//alert(getCookie("pkPm"));
	window.location.href="/partyBuilding/back/infoManage/partyMen.html";

}

/*22.	查看视频详情*/

function viewVideo(node){
	var pkVedio = node.getAttribute("pkVedio");
	setCookie("pkVedio",pkVedio);
	window.location.href="/partyBuilding/back/infoManage/video.html";

}


//党员
function info(node){
	//alert("aaaaa");
	var pkPartyMem = node.getAttribute("pkPartyMem");
    setCookie("pkPartyMem",pkPartyMem);
    //alert(getCookie("pkPartyMem"));
    window.location.href="/partyBuilding/prev/parMem/intro.html";
}

/*7.	查看信息详情*/

function viewArt(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="/partyBuilding/back/infoManage/infomation.html";

}

/*1.	查询所有主栏目*/
function searchMcol(){
$.ajax({
	type : "GET",
	url : "/partyBuilding/mcol" ,
	contentType : "application/json",
	dataType : "json",
	success : function(data) {
		//alert(JSON.stringify(data));
		
		if(data.code == "200") {
			$.each(data.mCols, function(num, mCols) {
				//alert(mCols.mcolName);
			$("#mcol").append("<option value='"+mCols.pkMcol+"'>"+mCols.mcolName+"</option>")
		});
		}
	},
	error : function() {
		
	}
});  

}

/*2.	根据主栏目查询其子栏目*/
function searchScol(){
	$("#mcol").bind("change",function(){
		var pkMcol = $("#mcol").val();

		$.ajax({
			type : "GET",
			url : "/partyBuilding/scol/mcol/"+pkMcol,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
				//alert(JSON.stringify(data));
				
				if(data.code == "200") {
					$("#scol").html("");
					$("#scol").append("<option value='000'>点击选择...</option>");
					$.each(data.sCol, function(num, sCols) {
					$("#scol").append("<option value='"+sCols.pkScol+"'>"+sCols.scolName+"</option>")
				});
				}
			},
			error : function() {
				
			}
		});  
		});
}

/*显示子栏目*/
function showScol(fkScol){
	var pkMcol = $("#mcol").val();

	$.ajax({
		type : "GET",
		url : "/partyBuilding/scol/mcol/"+pkMcol,
		contentType : "application/json",
		dataType : "json",
		success : function(data) {
			//alert(JSON.stringify(data));
			
			if(data.code == "200") {
				$("#scol").html("");
				$("#scol").append("<option value='000'>点击选择...</option>");
				$.each(data.sCol, function(num, sCols) {
				$("#scol").append("<option value='"+sCols.pkScol+"'>"+sCols.scolName+"</option>")
			});
				$("#ssscol").find("option[value='"+fkScol+"']").attr("selected","selected");
				searchScol();
			}
		},
		error : function() {
			
		}
	});  
}

function getRadioValue(radioName) {        
	 var chkRadio = document.getElementsByName(radioName);        
	 for (var i = 0; i < chkRadio.length; i++) {          
	 if (chkRadio[i].checked)            
		 return chkRadio[i].value;        }      
	 }

/*30.	根据主栏目分页查询所有子栏目*/

function scolList(){
	$("#mcol").bind("change",function(){
		var pkMcol = $("#mcol").val();
		
		$.ajax({
			type : "GET",
			url : "/partyBuilding/scol/mcol/"+pkMcol+"/curPage/1/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
				//alert(JSON.stringify(data));
				if(data.code == "200") {
					var pag = data.pageInfo.totalCount; //信息列表总记录数目
					pagination(pag, 1);
					showPro(data);
				}
			},
			error : function() {
				
			}
		});  
		});
}

/*注销*/
function loginout(){
	//alert("注销");
	$.ajax({
		type : "GET",
		url : "/partyBuilding/admin/logout" ,
		contentType : "application/json",
		dataType : "json",
		success : function(data) {
			//alert(JSON.stringify(data));
			if(data.code == "200") {
				window.location.href="/partyBuilding/back/login/login.html";
			}
		},
		error : function() {
			
		}
	});  
}

function notCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../parTre/notCont.html";
}

function newsCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../parTre/newsCont.html";
}
function hisCont(node){
	var pkVedio = node.getAttribute("pkVedio");
	setCookie("pkVedio",pkVedio);
	window.location.href="../parVideo/hisCont.html";
}
function repCont(node){
	var pkVedio = node.getAttribute("pkVedio");
	setCookie("pkVedio",pkVedio);
	window.location.href="../parVideo/repCont.html";
}
function disCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../parSys/disCont.html";
}
function testCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../onlSch/testCont.html";
}
function fabCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../onlSch/fabCont.html";
}

function litCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../onlSch/litCont.html";
}
function conCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../onlSch/conCont.html";
}
function chaCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../innoGood/chaCont.html";
}
function leaCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../innoGood/leaCont.html";
}
function actCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="../innoGood/actCont.html";
}

function seaCont(node){
	var pkArticle = node.getAttribute("pkArticle");
	setCookie("pkArticle",pkArticle);
	window.location.href="seaCont.html";
}

function seaVid(node){
	var pkVedio = node.getAttribute("pkVedio");
	setCookie("pkVedio",pkVedio);
	window.location.href="seaVid.html";
}







