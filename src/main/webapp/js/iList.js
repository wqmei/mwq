

function PageBtn(){
	
	/* 分页数字跳转  */
	$(".start").on("click",function(e){
	var dataPage = $(this).attr("data-page");
		$.ajax({
		type : "GET",
		url : "/partyBuilding/article/curPage/"+dataPage+"/pageSize/10" ,
		contentType : "application/json",
		dataType : "json",
		success : function(data) {
		ALERT(JSON.stringify(data));
		if(data.code == "200") {
		var pag = data.pageInfo.totalCount; //信息列表总记录数目
		showList(data);
		PageBtn();
		} else {
			ALERT(data.msg);
			alertModal("警告", data.msg);
			}
		},
			error : function() {
			handleAjaxError();
					}
			});   
	})
	
	/* 跳转按钮跳转 */
	$("#jumpBtn").on("click",function(e){

		var dataPage = $("#jumpIpt").val();
			$.ajax({
			type : "GET",
			url : "/partyBuilding/article/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showList(data);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
		});   
	})
	
	/* 下一页按钮跳转 */
	$("#nextContent").on("click",function(e){
		var curPage = parseInt($(".active").text());
		var dataPage = curPage +1 ; 
			$.ajax({
			type : "GET",
			url : "/partyBuilding/article/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showList(data);
			pagination(pag,dataPage);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
			/* 上一页按钮跳转 */
	$("#prevContent").on("click",function(e){
		var curPage = parseInt($(".active").text());
		var dataPage = curPage -1 ; 
			$.ajax({
			type : "GET",
			url : "/partyBuilding/article/curPage/"+dataPage+"/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showList(data);
			pagination(pag,dataPage);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
				/* 首页按钮跳转 */
	$("#home").on("click",function(e){
			$.ajax({
			type : "GET",
			url : "/partyBuilding/article/curPage/1/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showList(data);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			
			/* 末页按钮跳转 */
	$("#end").on("click",function(e){
			$.ajax({
			type : "GET",
			url :"/partyBuilding/article/curPage/" + totalPage + "/pageSize/10" ,
			contentType : "application/json",
			dataType : "json",
			success : function(data) {
			ALERT(JSON.stringify(data));
			if(data.code == "200") {
			var pag = data.pageInfo.totalCount; //信息列表总记录数目
			showList(data);
			PageBtn();
			} else {
				ALERT(data.msg);
				alertModal("警告", data.msg);
				}
			},
			error : function() {
			handleAjaxError();
				}
			});  

			})
			

 }
 
