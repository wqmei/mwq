 $(document).ready(function(){
	 left_nav();
        })


function left_nav(){
  
    var side_menu_Html =   '<div class="am-offcanvas-bar admin-offcanvas-bar" >' +
    '<ul class="am-list admin-sidebar-list">' +

     '<li class="admin-parent">' +
       '<a class="am-cf" data-am-collapse="{target: \'#collapse-nav-1\'}"> 信息发布 <span class="am-fr am-margin-right">▼</span></a>' +
       '<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-nav-1">' +
         '<li><a href="../infoPublish/pubInfo.html" >发布信息</a></li>' +
         '<li><a href="../infoPublish/pubVideo.html"> 发布视频</a></li>' +
        '<li><a href="../infoPublish/addMember.html"> 添加党员</a></li>' +
        '</ul>' +
     '</li>' +
		
		
		 '<li class="admin-parent">' +
       '<a class="am-cf" data-am-collapse="{target: \'#collapse-nav-2\'}"> 信息管理 <span class="am-fr am-margin-right">▼</span></a>' +
       '<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-nav-2">' +
        '<li><a href="../infoManage/infoList.html"> 信息列表</a></li>' +
        '<li><a href="../infoManage/memberList.html"> 党员列表</a></li>' +
        '<li><a href="../infoManage/videoList.html"> 视频列表</a></li>' +
       '</ul>' +
     '</li>' +
     
     '<li class="admin-parent">' +
     '<a class="am-cf" data-am-collapse="{target: \'#collapse-nav-3\'}"> 栏目管理 <span class="am-fr am-margin-right">▼</span></a>' +
     '<ul class="am-list am-collapse admin-sidebar-sub" id="collapse-nav-3">' +
      '<li><a href="../proManage/mainPro.html"> 主栏目</a></li>' +
     '<li><a href="../proManage/subPro.html"> 子栏目</a></li>' +
        '</ul>' +
   '</li>' +

   '</ul>' +

   '</br></br></br></br></br></br>' +
 '</div>';
   

    $(".am-offcanvas").html(side_menu_Html);
}
 
 
/* 全选按钮*/
 var CheckBox=document.getElementsByTagName('input');
$("#all_btn").on("click",function(){
	for(i=0;i<CheckBox.length-1;i++){
	CheckBox[i].checked=true;
	};
})

/*反选按钮*/
$("#reall_btn").on("click",function(){
for(i=0;i<CheckBox.length-1;i++){
   if(CheckBox[i].checked==true){
     CheckBox[i].checked=false;
    }
    else{
       CheckBox[i].checked=true;
       }
                    
    };
});

/*信息批量删除*/
$("#rtn_btn").on("click",function(){
	var del=[];
	for(i=0;i<CheckBox.length-1;i++){
		   if(CheckBox[i].checked==true){
			   del[i]=CheckBox[i].value;
		   }
		   }
	$.ajax({
		type : "DELETE",
		url : "/partyBuilding/articles" ,
		contentType : "application/json",
		data:encrypt(del),
		dataType : "json",
		
		success : function(data) {
			location.reload();
		},
			error : function(data) {
				
					}
			});   
	
})

/*党员批量删除*/
$("#rtn_btn_mem").on("click",function(){
	var del=[];
	for(i=0;i<CheckBox.length-1;i++){
		   if(CheckBox[i].checked==true){
			   del[i]=CheckBox[i].value;
		   }
		   }
	$.ajax({
		type : "DELETE",
		url : "/partyBuilding/partyMems" ,
		contentType : "application/json",
		data:encrypt(del),
		dataType : "json",
		
		success : function(data) {
			location.reload();
		},
			error : function(data) {
				
					}
			});   
	
})

/*视频批量删除*/
$("#rtn_btn").on("click",function(){
	var del=[];
	for(i=0;i<CheckBox.length-1;i++){
		   if(CheckBox[i].checked==true){
			   del[i]=CheckBox[i].value;
		   }
		   }
	$.ajax({
		type : "DELETE",
		url : "/partyBuilding/vedios" ,
		contentType : "application/json",
		data:encrypt(del),
		dataType : "json",
		
		success : function(data) {
			location.reload();
		},
			error : function(data) {
				
					}
			});   
	
})

