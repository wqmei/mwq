package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.SColumn;

public interface SColumnMapper {
    int deleteByPrimaryKey(Integer pkScol);

    int insert(SColumn record);

    int insertSelective(SColumn record);

    SColumn selectByPrimaryKey(Integer pkScol);

    int updateByPrimaryKeySelective(SColumn record);

    int updateByPrimaryKey(SColumn record);
    
    List<SColumn> selectAll();
    
    List<SColumn> selectByMCol(int pkMCol);
}