package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.PartyMember;

public interface PartyMemberMapper {
    int deleteByPrimaryKey(Integer pkPm);

    int insert(PartyMember record);

    int insertSelective(PartyMember record);

    PartyMember selectByPrimaryKey(Integer pkPm);

    int updateByPrimaryKeySelective(PartyMember record);

    int updateByPrimaryKey(PartyMember record);
    
    List<PartyMember> selectAll();
    
    List<PartyMember> selet5Randomly();
    
    List<PartyMember> selectByName(String content);
}