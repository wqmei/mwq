package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.MColumn;

public interface MColumnMapper {
    int deleteByPrimaryKey(Integer pkMcol);

    int insert(MColumn record);

    int insertSelective(MColumn record);

    MColumn selectByPrimaryKey(Integer pkMcol);

    int updateByPrimaryKeySelective(MColumn record);

    int updateByPrimaryKey(MColumn record);
    
    List<MColumn> selectAll();
}