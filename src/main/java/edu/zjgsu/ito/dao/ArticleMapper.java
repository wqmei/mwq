package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.Article;

public interface ArticleMapper {
    int deleteByPrimaryKey(Integer pkArticle);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Integer pkArticle);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);
    
    List<Article> selectAll();
    
    List<Article> selectByMCol(int pkMCol);
    
    List<Article> selectByName(String artTitle);
    
    List<Article> selectBySCol(int pkSCol);
}