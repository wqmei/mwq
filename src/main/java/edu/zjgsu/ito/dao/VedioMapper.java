package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.Vedio;

public interface VedioMapper {
    int deleteByPrimaryKey(Integer pkVedio);

    int insert(Vedio record);

    int insertSelective(Vedio record);

    Vedio selectByPrimaryKey(Integer pkVedio);

    int updateByPrimaryKeySelective(Vedio record);

    int updateByPrimaryKey(Vedio record);
    
    List<Vedio> selectAll();
    
    List<Vedio> selectByScol(int pkScol);
    
    List<Vedio> selectByMcol(int pkMcol);
    
    List<Vedio> selectByName(String content);
}