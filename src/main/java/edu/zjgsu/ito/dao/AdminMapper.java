package edu.zjgsu.ito.dao;

import java.util.List;

import edu.zjgsu.ito.model.Admin;

public interface AdminMapper {
    int deleteByPrimaryKey(Integer pkAdm);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer pkAdm);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

	List<Admin> selectAll();
	
	Admin selectByAdmName(String admName);
}