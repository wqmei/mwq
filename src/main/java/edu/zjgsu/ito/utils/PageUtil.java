package edu.zjgsu.ito.utils;

import java.util.HashMap;
import java.util.Map;

import edu.zjgsu.ito.model.Page;

/**
 * 分页工具类
 * @author Jenson_Zhou
 *
 */
public class PageUtil {

	/**
	 * 将page对象中的分页信息放入map中
	 * @param page
	 * @param map
	 */
	public static void putPageInfo(Page page, Map<String, Object> map) {
		Map<String, Integer> pageInfo = new HashMap<String, Integer>();
		pageInfo.put("curPage", page.getCurPage());
		pageInfo.put("pageSize", page.getPageSize());
		pageInfo.put("totalCount", page.getTotalCount());
		map.put("pageInfo", pageInfo);
	}
}
