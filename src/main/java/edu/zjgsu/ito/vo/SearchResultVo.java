package edu.zjgsu.ito.vo;

import java.sql.Timestamp;

/**
 * 标题搜索vo
 * @author sjjok
 *
 */
public class SearchResultVo {

	String type;
	
	int id;
	
	String title;
	
	Timestamp time;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	
}
