package edu.zjgsu.ito.vo;

import javax.validation.constraints.NotNull;

public class AdminLoginVo {

	@NotNull
    private String admName;

	@NotNull
    private String admPwd;

	public String getAdmName() {
		return admName;
	}

	public void setAdmName(String admName) {
		this.admName = admName;
	}

	public String getAdmPwd() {
		return admPwd;
	}

	public void setAdmPwd(String admPwd) {
		this.admPwd = admPwd;
	}

    
}
