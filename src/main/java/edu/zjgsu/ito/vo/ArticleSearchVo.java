package edu.zjgsu.ito.vo;

import java.util.Date;

public class ArticleSearchVo {

	private Integer pkArticle;
	
	private String artTitle;
	
	private Date artCreateTime;

	public Integer getPkArticle() {
		return pkArticle;
	}

	public void setPkArticle(Integer pkArticle) {
		this.pkArticle = pkArticle;
	}

	public String getArtTitle() {
		return artTitle;
	}

	public void setArtTitle(String artTitle) {
		this.artTitle = artTitle;
	}

	public Date getArtCreateTime() {
		return artCreateTime;
	}

	public void setArtCreateTime(Date artCreateTime) {
		this.artCreateTime = artCreateTime;
	}
	
	
}
