package edu.zjgsu.ito.vo;

import java.util.Date;

public class PartyMemSearchVo {

	private Integer pkPm;
	
	private String pmName;
	
	private Date pmJoinDate;

	public Integer getPkPm() {
		return pkPm;
	}

	public void setPkPm(Integer pkPm) {
		this.pkPm = pkPm;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public Date getPmJoinDate() {
		return pmJoinDate;
	}

	public void setPmJoinDate(Date pmJoinDate) {
		this.pmJoinDate = pmJoinDate;
	}
	
	
}
