package edu.zjgsu.ito.vo;

import java.sql.Timestamp;

public class AdminShowVo {

	private Integer pkAdm;
	
	private String admName;
	
	private Timestamp admCreateTime;

	public Integer getPkAdm() {
		return pkAdm;
	}

	public void setPkAdm(Integer pkAdm) {
		this.pkAdm = pkAdm;
	}

	public String getAdmName() {
		return admName;
	}

	public void setAdmName(String admName) {
		this.admName = admName;
	}

	public Timestamp getAdmCreateTime() {
		return admCreateTime;
	}

	public void setAdmCreateTime(Timestamp admCreateTime) {
		this.admCreateTime = admCreateTime;
	}
	
	
}
