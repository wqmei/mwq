package edu.zjgsu.ito.vo;

import java.util.Date;

public class VedioSearchVo {

	private Integer pkVedio;
	
	private String vedioTitle;
	
	private Date vedioUploadTime;

	public Integer getPkVedio() {
		return pkVedio;
	}

	public void setPkVedio(Integer pkVedio) {
		this.pkVedio = pkVedio;
	}

	public String getVedioTitle() {
		return vedioTitle;
	}

	public void setVedioTitle(String vedioTitle) {
		this.vedioTitle = vedioTitle;
	}

	public Date getVedioUploadTime() {
		return vedioUploadTime;
	}

	public void setVedioUploadTime(Date vedioUploadTime) {
		this.vedioUploadTime = vedioUploadTime;
	}
	
	
}
