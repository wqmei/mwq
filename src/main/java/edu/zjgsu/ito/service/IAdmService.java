package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.Admin;
import edu.zjgsu.ito.vo.AdminLoginVo;
import edu.zjgsu.ito.vo.AdminShowVo;

public interface IAdmService {

	/**
	 * 根据管理员id查询管理员
	 * @param id
	 * @return
	 */
	public AdminShowVo getAdm(int id);
	
	/**
	 * 根据id查询管理员详情
	 * @param id
	 * @return
	 */
	public Admin getAdmDetail(int id);
	
	/**
	 * 查询所有管理员
	 * @return
	 */
	public List<AdminShowVo> getAdms();
	
	/**
	 * 新增管理员
	 * @param admin
	 * @return
	 */
	public int insertAdm(Admin admin);
	
	/**
	 * 根据管理员id删除管理员
	 * @param id
	 * @return
	 */
	public int deleteAdm(int id);
	
	/**
	 * 更新管理员
	 * @param admin
	 * @return
	 */
	public int updateAdm(Admin admin);
	
	/**
	 * 管理员登录
	 * @param adm
	 * @return
	 */
	public int admLogin(AdminLoginVo adm);
	
	/**
	 * 管理员重置密码
	 * @return 
	 */
	public Boolean admReset();
}
