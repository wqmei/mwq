package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.vo.ArticleSearchVo;

public interface IArticleService {

	/**
	 * 根据主栏目查询所有信息
	 * @param pkMCol
	 * @return
	 */
	public List<Article> getArticleByMCol(int pkMCol);
	
	/**
	 * 查看信息详情
	 * @param id
	 * @return
	 */
	public Article getArticle(int id);

	/**
	 * 查询所有信息
	 * @return
	 */
	public List<Article> getArticles();
    
	/**
	 * 新增信息
	 * @param article
	 * @return
	 */
	public int insertArticle(Article article);

	/**
	 * 删除信息
	 * @param id
	 * @return
	 */
    public int delete(int id);

    /**
     * 修改信息
     * @param article
     * @return
     */
    public int updateArticle(Article article);

    /**
     * 分页查询所有信息
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<Article> getArticleByPage(int curPage, int PageSize);

    /**
     * 根据主栏目分页查询所有信息
     * @param pkMcol
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<Article> getArticlePageByMColumn(int pkMcol,int curPage, int PageSize);

    /**
     * 根据子栏目分页查询所有信息
     * @param pkScol
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<Article> getArticleBySColumn(int pkScol,int curPage, int pageSize);
    
    /**
     * 根据标题查询
     * @param content
     * @return
     */
    public List<ArticleSearchVo> getArticleByName(String content);
}
