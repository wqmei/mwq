package edu.zjgsu.ito.service;

import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

	/**
	 * 上传头像
	 * @param image 头像图片
	 * @return 存储文件路径
	 */
	public String uploadHead(MultipartFile image);
	
	/**
	 * 上传视频
	 * @param video
	 * @return
	 */
	public String uploadVideo(MultipartFile video);
}
