package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.Vedio;
import edu.zjgsu.ito.vo.VedioSearchVo;

public interface IVedioService {

	/**
	 * 新增视频
	 * @param vedio
	 * @return
	 */
	public int insertVedio(Vedio vedio);
	
	/**
	 * 删除视频
	 * @param id
	 * @return
	 */
	public int deleteVedio(int id);
	
	/**
	 * 修改视频
	 * @param vedio
	 * @return
	 */
	public int updateVedio(Vedio vedio);
	
	/**
	 * 查看视频详情
	 * @param id
	 * @return
	 */
	public Vedio getVedio(int id);
	
	/**
	 * 根据子栏目分页查询所有视频
	 * @param pkScol
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	public Page<Vedio> getVedioPageByScol(int pkScol, int curPage, int pageSize);
	
	/**
	 * 根据主栏目分页查询所有视频
	 * @param pkMcol
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	public Page<Vedio> getVedioPageByMcol(int pkMcol, int curPage, int pageSize);
	
	/**
	 * 分页查询视频列表
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	public Page<Vedio> getVedioByPage(int curPage, int pageSize);
	
	/**
	 * 根据视频标题查询视频
	 * @param content
	 * @return
	 */
	public List<VedioSearchVo> getVedioByName(String content);
}
