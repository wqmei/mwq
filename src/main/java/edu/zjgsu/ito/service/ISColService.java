package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.SColumn;

public interface ISColService {

	/**
	 * 根据主栏目查询其子栏目
	 * @param pkMCol
	 * @return
	 */
	public List<SColumn> getSColByMCol(int pkMCol);
	
	/**
	 * 查看子栏目详情
	 * @param id
	 * @return
	 */
	public SColumn getSColumn(int id);

	/**
	 * 查询所有子栏目
	 * @return
	 */
	public List<SColumn> getSColumns();

	/**
	 * 新增子栏目
	 * @param SColumn
	 * @return
	 */
	public int insertSColumn(SColumn SColumn);

	/**
	 * 删除子栏目
	 * @param id
	 * @return
	 */
    public int delete(int id);
    
    /**
     * 修改子栏目
     * @param SColumn
     * @return
     */
    int updateSColumn(SColumn SColumn);

    /**
     * 分页查询所有子栏目
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<SColumn> getSColumnByPage(int curPage, int PageSize);
    
    /**
     * 根据主栏目分页查询子栏目
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<SColumn> getSColPageByMCol(int curPage, int PageSize, int pkMCol);
}
