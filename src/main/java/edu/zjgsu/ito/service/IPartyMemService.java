package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.PartyMember;
import edu.zjgsu.ito.vo.PartyMemSearchVo;

public interface IPartyMemService {

	/**
	 * 新增党员
	 * @param partyMem
	 * @return
	 */
	public int insertPartyMem(PartyMember partyMem);
	
	/**
	 * 党员详情
	 * @param id
	 * @return
	 */
	public PartyMember getPartyMem(int id);
	
	/**
	 * 删除党员
	 * @param id
	 * @return
	 */
	public int deletePartyMem(int id);
	
	/**
	 * 修改党员
	 * @param partyMem
	 * @return
	 */
	public int updatePartyMem(PartyMember partyMem);
	
	/**
	 * 分页查询所有党员
	 * @param curPage
	 * @param PageSize
	 * @return
	 */
	public Page<PartyMember> getPartyMemByPage(int curPage, int pageSize);
	
	/**
	 * 随机查询五名党员
	 * @return
	 */
	public List<PartyMember> get5PartyMemRandomly();
	
	/**
	 * 根据党员名字查询党员
	 * @param content
	 * @return
	 */
	public List<PartyMemSearchVo> getPartyMemByName(String content);
}
