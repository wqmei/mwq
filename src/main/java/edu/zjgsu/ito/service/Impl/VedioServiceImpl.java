package edu.zjgsu.ito.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.VedioMapper;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.Vedio;
import edu.zjgsu.ito.service.IVedioService;
import edu.zjgsu.ito.utils.FileUtil;
import edu.zjgsu.ito.vo.VedioSearchVo;

@Service("vedioService")
@Transactional
public class VedioServiceImpl implements IVedioService {

	@Autowired
	VedioMapper vedioMapper;
	
	public int insertVedio(Vedio vedio) {
		vedio.setVedioUploadTime(new Timestamp(System.currentTimeMillis()));
		/*String destPath = FileUtil.savePhoto(vedio.getVedioUrl());
		vedio.setVedioUrl(destPath);*/
		return vedioMapper.insertSelective(vedio);
	}

	public int deleteVedio(int id) {
		return vedioMapper.deleteByPrimaryKey(id);
	}

	public int updateVedio(Vedio vedio) {
		/*String destPath = FileUtil.savePhoto(vedio.getVedioUrl());
		vedio.setVedioUrl(destPath);*/
		return vedioMapper.updateByPrimaryKeySelective(vedio);
	}

	public Vedio getVedio(int id) {
		return vedioMapper.selectByPrimaryKey(id);
	}

	public Page<Vedio> getVedioPageByScol(int pkScol, int curPage, int pageSize) {
		List<Vedio> vedios = vedioMapper.selectByScol(pkScol);
		Page<Vedio> vedioPage = new Page<Vedio>();
		
		List<Vedio> resultVedio = new ArrayList<Vedio>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>vedios.size()?vedios.size():curPage*pageSize); i++) {
			resultVedio.add(vedios.get(i));
		}
		
		vedioPage.setEntities(resultVedio);
		vedioPage.setCurPage(curPage);
		vedioPage.setPageSize(pageSize);
		vedioPage.setTotalCount(vedios.size());
		return vedioPage;
	}

	public Page<Vedio> getVedioPageByMcol(int pkMcol, int curPage, int pageSize) {
		List<Vedio> vedios = vedioMapper.selectByMcol(pkMcol);
		Page<Vedio> vedioPage = new Page<Vedio>();
		
		List<Vedio> resultVedio = new ArrayList<Vedio>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>vedios.size()?vedios.size():curPage*pageSize); i++) {
			resultVedio.add(vedios.get(i));
		}
		
		vedioPage.setEntities(resultVedio);
		vedioPage.setCurPage(curPage);
		vedioPage.setPageSize(pageSize);
		vedioPage.setTotalCount(vedios.size());
		return vedioPage;
	}

	public Page<Vedio> getVedioByPage(int curPage, int pageSize) {
		List<Vedio> vedios = vedioMapper.selectAll();
		Page<Vedio> vedioPage = new Page<Vedio>();
		
		List<Vedio> resultVedio = new ArrayList<Vedio>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>vedios.size()?vedios.size():curPage*pageSize); i++) {
			resultVedio.add(vedios.get(i));
		}
		
		vedioPage.setEntities(resultVedio);
		vedioPage.setCurPage(curPage);
		vedioPage.setPageSize(pageSize);
		vedioPage.setTotalCount(vedios.size());
		return vedioPage;
	}

	public List<VedioSearchVo> getVedioByName(String content) {
		List<Vedio> videos = vedioMapper.selectByName(content);
		List<VedioSearchVo> videoVos = new ArrayList<VedioSearchVo>();
		for(Vedio video : videos) {
			VedioSearchVo videoVo = new VedioSearchVo();
			BeanUtils.copyProperties(video, videoVo);
			videoVos.add(videoVo);
		}
		return videoVos;
	}

}
