package edu.zjgsu.ito.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.SColumnMapper;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.SColumn;
import edu.zjgsu.ito.service.ISColService;

@Service("sColService")
@Transactional
public class SColServiceImpl implements ISColService {

	@Autowired
	private SColumnMapper sColMapper;
	
	public List<SColumn> getSColByMCol(int pkMCol) {
		return sColMapper.selectByMCol(pkMCol);
	}

	public SColumn getSColumn(int id) {
		return sColMapper.selectByPrimaryKey(id);
	}

	public List<SColumn> getSColumns() {
		return sColMapper.selectAll();
	}



	public int insertSColumn(SColumn SColumn) {
		return sColMapper.insertSelective(SColumn);
	}

	public int delete(int id) {
		return sColMapper.deleteByPrimaryKey(id);
	}

	public int updateSColumn(SColumn SColumn) {
		return sColMapper.updateByPrimaryKeySelective(SColumn);
	}

	public Page<SColumn> getSColumnByPage(int curPage, int pageSize) {
		List<SColumn> sColumns = sColMapper.selectAll();
		Page<SColumn> sColumnPage = new Page<SColumn>();
		sColumnPage.setEntities(sColumns);
		sColumnPage.setCurPage(curPage);
		sColumnPage.setPageSize(pageSize);
		sColumnPage.setTotalCount(sColumns.size());
		return sColumnPage;

	}

	public Page<SColumn> getSColPageByMCol(int curPage, int pageSize, int pkMCol) {
		List<SColumn> sColumns = sColMapper.selectByMCol(pkMCol);
		Page<SColumn> sColumnPage = new Page<SColumn>();
		sColumnPage.setEntities(sColumns);
		sColumnPage.setCurPage(curPage);
		sColumnPage.setPageSize(pageSize);
		sColumnPage.setTotalCount(sColumns.size());
		return sColumnPage;
	}
}
