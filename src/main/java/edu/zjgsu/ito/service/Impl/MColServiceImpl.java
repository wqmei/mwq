package edu.zjgsu.ito.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.MColumnMapper;
import edu.zjgsu.ito.model.MColumn;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.service.IMColService;

@Service("mColService")
@Transactional
public class MColServiceImpl implements IMColService{

	@Autowired
	private MColumnMapper mColMapper;

	public MColumn getMColumn(int id) {
		return mColMapper.selectByPrimaryKey(id);
	}

	public List<MColumn> getMColumns() {
		return mColMapper.selectAll();
	}

	public int insertMColumn(MColumn mcolumn) {
		return mColMapper.insertSelective(mcolumn);
	}

	public int deleteMColumn(int id) {
		return mColMapper.deleteByPrimaryKey(id);
	}

	public int updateMColumn(MColumn mcolumn) {
		return mColMapper.updateByPrimaryKeySelective(mcolumn);
	}

	public Page<MColumn> getMColumnByPage(int curPage, int pageSize) {
		List<MColumn> mColumns = mColMapper.selectAll();
		Page<MColumn> mColumnPage = new Page<MColumn>();
		mColumnPage.setEntities(mColumns);
		mColumnPage.setCurPage(curPage);
		mColumnPage.setPageSize(pageSize);
		mColumnPage.setTotalCount(mColumns.size());
		return mColumnPage;

	}
	
	
}
