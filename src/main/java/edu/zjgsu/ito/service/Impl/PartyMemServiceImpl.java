package edu.zjgsu.ito.service.Impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.PartyMemberMapper;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.PartyMember;
import edu.zjgsu.ito.service.IPartyMemService;
import edu.zjgsu.ito.utils.FileUtil;
import edu.zjgsu.ito.vo.PartyMemSearchVo;

@Service("partyMemService")
@Transactional
public class PartyMemServiceImpl implements IPartyMemService {

	@Autowired
	PartyMemberMapper partyMemMapper;
	
	public int insertPartyMem(PartyMember partyMem) {
		/*String destPath = FileUtil.savePhoto(partyMem.getPmPhotoUrl());
		partyMem.setPmPhotoUrl(destPath);*/
		return partyMemMapper.insertSelective(partyMem);
	}

	public PartyMember getPartyMem(int id) {
		return partyMemMapper.selectByPrimaryKey(id);
	}

	public int deletePartyMem(int id) {
		return partyMemMapper.deleteByPrimaryKey(id);
	}

	public int updatePartyMem(PartyMember partyMem) {
		/*String destPath = FileUtil.savePhoto(partyMem.getPmPhotoUrl());
		partyMem.setPmPhotoUrl(destPath);*/
		return partyMemMapper.updateByPrimaryKeySelective(partyMem);
	}

	public Page<PartyMember> getPartyMemByPage(int curPage, int pageSize) {
		List<PartyMember> partyMembers = partyMemMapper.selectAll();
		Page<PartyMember> partyMemPage = new Page<PartyMember>();
		
		List<PartyMember> resultPM = new ArrayList<PartyMember>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>partyMembers.size()?partyMembers.size():curPage*pageSize); i++) {
			resultPM.add(partyMembers.get(i));
		}
		
		partyMemPage.setEntities(resultPM);
		partyMemPage.setCurPage(curPage);
		partyMemPage.setPageSize(pageSize);
		partyMemPage.setTotalCount(partyMembers.size());
		return partyMemPage;
	}

	public List<PartyMember> get5PartyMemRandomly() {
		return partyMemMapper.selet5Randomly();
	}

	public List<PartyMemSearchVo> getPartyMemByName(String content) {
		List<PartyMember> partyMems = partyMemMapper.selectByName(content);
		List<PartyMemSearchVo> partyMemVos = new ArrayList<PartyMemSearchVo>();
		for(PartyMember partyMem : partyMems) {
			PartyMemSearchVo partyMemVo = new PartyMemSearchVo();
			BeanUtils.copyProperties(partyMem, partyMemVo);
			partyMemVos.add(partyMemVo);
		}
		return partyMemVos;
	}

}
