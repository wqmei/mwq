package edu.zjgsu.ito.service.Impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.AdminMapper;
import edu.zjgsu.ito.model.Admin;
import edu.zjgsu.ito.service.IAdmService;
import edu.zjgsu.ito.utils.Constant;
import edu.zjgsu.ito.utils.Md5Util;
import edu.zjgsu.ito.vo.AdminLoginVo;
import edu.zjgsu.ito.vo.AdminShowVo;

@Service("admService")
@Transactional
public class AdmServiceImpl implements IAdmService {

	@Autowired
	private AdminMapper admMapper;
	
	public AdminShowVo getAdm(int id) {
		AdminShowVo adm = new AdminShowVo();
		BeanUtils.copyProperties(admMapper.selectByPrimaryKey(id), adm);
		return adm;
	}
	
	public Admin getAdmDetail(int id) {
		return admMapper.selectByPrimaryKey(id);
	}

	public List<AdminShowVo> getAdms() {
		List<AdminShowVo> admVos = new ArrayList<AdminShowVo>();
		List<Admin> adms = admMapper .selectAll();
		for(Admin adm : adms) {
			AdminShowVo admVo = new AdminShowVo();
			BeanUtils.copyProperties(adm, admVo);
			admVos.add(admVo);
		}
		return admVos;
	}

	public int insertAdm(Admin admin) {
		admin.setAdmCreateTime(new Timestamp(System.currentTimeMillis()));
		admin.setAdmPwd(Md5Util.getMD5(admin.getAdmPwd()));
		return admMapper.insertSelective(admin);
	}

	public int deleteAdm(int id) {
		return admMapper.deleteByPrimaryKey(id);
	}

	public int updateAdm(Admin admin) {
		admin.setAdmPwd(Md5Util.getMD5(admin.getAdmPwd()));
		return admMapper.updateByPrimaryKeySelective(admin);
	}

	public int admLogin(AdminLoginVo adm) {
		adm.setAdmPwd(Md5Util.getMD5(adm.getAdmPwd()));
		if(admMapper.selectByAdmName(adm.getAdmName()) == null) {
			return Constant.USER_NOT_EXIST;
		}
		else {
			Admin admin = admMapper.selectByAdmName(adm.getAdmName());
			if(admin.getAdmPwd().equals(adm.getAdmPwd())) {
				return Constant.LOGIN_SUCCESS;
			}
			else {
				return Constant.WRONG_PASSWORD;
			}
		}
	}
	
	public Boolean admReset() {
		Properties prop = new Properties();
		try {
			prop.load(AdmServiceImpl.class.getClassLoader().getResourceAsStream("default.properties"));
			//System.out.println("============================="+prop.getProperty("defaultPwd"));
			if(prop.getProperty("flag").equals("1")) {
				Admin admin = getAdmDetail(1);
				admin.setAdmPwd(Md5Util.getMD5(prop.getProperty("defaultPwd")));
				//System.out.println("============================"+prop.getProperty("defaultPwd"));
				admMapper.updateByPrimaryKeySelective(admin);
				FileOutputStream fos=new FileOutputStream("default.properties");
				prop.setProperty("flag", "0");
				prop.store(fos, null);
				//System.out.println("============================" + prop.getProperty("flag"));
				fos.close();
				return true;
			}
			else
				return false;
				
		} catch (IOException e) {
			throw new RuntimeException("找不到配置文件");
		}
	}

}
