package edu.zjgsu.ito.service.Impl;

import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.zjgsu.ito.dao.ArticleMapper;
import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.service.IArticleService;
import edu.zjgsu.ito.utils.Constant;
import edu.zjgsu.ito.utils.FileUtil;
import edu.zjgsu.ito.vo.ArticleSearchVo;

@Service("articleService")
@Transactional
public class ArticleServiceImpl implements IArticleService{

	@Autowired
	private ArticleMapper articleMapper;

	public List<Article> getArticleByMCol(int pkMCol) {
		return articleMapper.selectByMCol(pkMCol);
	}

	public int insertArticle(Article article) {
		String articleUrl = FileUtil.getRandFileName(".txt");
		try {
			FileUtil.writeTxt(article.getArticleContent(), articleUrl, Constant.CONTENT_DIR);
			article.setArtCreateTime(new Timestamp(System.currentTimeMillis()));
			article.setArtContentUrl(articleUrl);
			return articleMapper.insertSelective(article);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("正文保存失败");
		}
	}
	
	public Article getArticle(int id) {
		Article article = articleMapper.selectByPrimaryKey(id);
		String url = article.getArtContentUrl();
		article.setArticleContent(FileUtil.readTxt(url));
		return article;
	}

	public List<Article> getArticles() {
		return articleMapper.selectAll();
	}

	public int delete(int id) {
		Article article = articleMapper.selectByPrimaryKey(id);
		FileUtil.deleteTxt(article.getArtContentUrl());
		return articleMapper.deleteByPrimaryKey(id);
	}

	public int updateArticle(Article article) {
		String articleUrl = article.getArtContentUrl();
		try {
			FileUtil.writeTxt(article.getArticleContent(), articleUrl, Constant.CONTENT_DIR);
			//article.setArtCreateTime(new Timestamp(System.currentTimeMillis()));
			return articleMapper.updateByPrimaryKeySelective(article);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("正文保存失败");
		}
	}

	public Page<Article> getArticleByPage(int curPage, int pageSize){
		List<Article> articles = articleMapper.selectAll();
		List<Article> resultArticles = new ArrayList<Article>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>articles.size()?articles.size():curPage*pageSize); i++) {
			resultArticles.add(articles.get(i));
		}
		
		Page<Article> articlePage = new Page<Article>();
		articlePage.setEntities(resultArticles);
		articlePage.setCurPage(curPage);
		articlePage.setPageSize(pageSize);
		articlePage.setTotalCount(articles.size());
		return articlePage;
	}

	public Page<Article> getArticlePageByMColumn(int pkMcol,int curPage, int pageSize){
		List<Article> articles = articleMapper.selectByMCol(pkMcol);
		Page<Article> articlePage = new Page<Article>();
		List<Article> resultArticles = new ArrayList<Article>();
		for(int i = (curPage-1)*pageSize; i < (curPage*pageSize>articles.size()?articles.size():curPage*pageSize); i++) {
			resultArticles.add(articles.get(i));
		}
		articlePage.setEntities(resultArticles);
		articlePage.setCurPage(curPage);
		articlePage.setPageSize(pageSize);
		articlePage.setTotalCount(articles.size());
		return articlePage;
	}

    public Page<Article> getArticleBySColumn(int pkScol,int curPage, int PageSize){
   	   List<Article> articles = articleMapper.selectBySCol(pkScol);
       Page<Article> articlePage = new Page<Article>();
       List<Article> resultArticles = new ArrayList<Article>();
		for(int i = (curPage-1)*PageSize; i < (curPage*PageSize>articles.size()?articles.size():curPage*PageSize); i++) {
			resultArticles.add(articles.get(i));
		}
	   articlePage.setEntities(resultArticles);
	   articlePage.setCurPage(curPage);
	   articlePage.setPageSize(PageSize);
	   articlePage.setTotalCount(articles.size());
	   return articlePage;
    }

	public List<ArticleSearchVo> getArticleByName(String content) {
		List<Article> articles = articleMapper.selectByName(content);
		List<ArticleSearchVo> articleVos = new ArrayList<ArticleSearchVo>();
		for(Article article : articles) {
			ArticleSearchVo articleVo = new ArticleSearchVo();
			BeanUtils.copyProperties(article, articleVo);
			articleVos.add(articleVo);
		}
		return articleVos;
	}
}
