package edu.zjgsu.ito.service;

import java.util.List;

import edu.zjgsu.ito.model.MColumn;
import edu.zjgsu.ito.model.Page;

public interface IMColService {
	
	/**
	 * 查看主栏目详情
	 * @param id
	 * @return
	 */
	MColumn getMColumn(int id);

	/**
	 * 查询所有主栏目
	 * @return
	 */
    List<MColumn> getMColumns();

    /**
     * 新增主栏目
     * @param mColumn
     * @return
     */
    int insertMColumn(MColumn mColumn);

    /**
     * 删除主栏目
     * @param id
     * @return
     */
    int deleteMColumn(int id);

    /**
     * 修改主栏目
     * @param mColumn
     * @return
     */
    int updateMColumn(MColumn mColumn);

    /**
     * 分页查询所有主栏目
     * @param curPage
     * @param PageSize
     * @return
     */
    public Page<MColumn> getMColumnByPage(int curPage, int PageSize);
}
