package edu.zjgsu.ito.model;

import java.util.Date;

public class PartyMember {
    private Integer pkPm;

    private String pmName;

    private String pmJob;

    private Date pmBirthDate;

    private Date pmJoinDate;

    private String pmIntro;

    private Boolean pmGender;

    private String pmPhotoUrl;

    public Integer getPkPm() {
        return pkPm;
    }

    public void setPkPm(Integer pkPm) {
        this.pkPm = pkPm;
    }

    public String getPmName() {
        return pmName;
    }

    public void setPmName(String pmName) {
        this.pmName = pmName == null ? null : pmName.trim();
    }

    public String getPmJob() {
        return pmJob;
    }

    public void setPmJob(String pmJob) {
        this.pmJob = pmJob == null ? null : pmJob.trim();
    }

    public Date getPmBirthDate() {
        return pmBirthDate;
    }

    public void setPmBirthDate(Date pmBirthDate) {
        this.pmBirthDate = pmBirthDate;
    }

    public Date getPmJoinDate() {
        return pmJoinDate;
    }

    public void setPmJoinDate(Date pmJoinDate) {
        this.pmJoinDate = pmJoinDate;
    }

    public String getPmIntro() {
        return pmIntro;
    }

    public void setPmIntro(String pmIntro) {
        this.pmIntro = pmIntro == null ? null : pmIntro.trim();
    }

    public Boolean getPmGender() {
        return pmGender;
    }

    public void setPmGender(Boolean pmGender) {
        this.pmGender = pmGender;
    }

    public String getPmPhotoUrl() {
        return pmPhotoUrl;
    }

    public void setPmPhotoUrl(String pmPhotoUrl) {
        this.pmPhotoUrl = pmPhotoUrl == null ? null : pmPhotoUrl.trim();
    }
}