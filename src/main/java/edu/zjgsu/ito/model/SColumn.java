package edu.zjgsu.ito.model;

import java.sql.Timestamp;

public class SColumn {
    private Integer pkScol;

    private String scolName;

    private String scolSum;

    private Integer fkMcol;
    
    private Timestamp addTime;

    public Integer getPkScol() {
        return pkScol;
    }

    public void setPkScol(Integer pkScol) {
        this.pkScol = pkScol;
    }

    public String getScolName() {
        return scolName;
    }

    public void setScolName(String scolName) {
        this.scolName = scolName == null ? null : scolName.trim();
    }

    public String getScolSum() {
        return scolSum;
    }

    public void setScolSum(String scolSum) {
        this.scolSum = scolSum == null ? null : scolSum.trim();
    }

    public Integer getFkMcol() {
        return fkMcol;
    }

    public void setFkMcol(Integer fkMcol) {
        this.fkMcol = fkMcol;
    }

	public Timestamp getAddTime() {
		return addTime;
	}

	public void setAddTime(Timestamp addTime) {
		this.addTime = addTime;
	}
    
}