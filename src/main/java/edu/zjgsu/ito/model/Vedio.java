package edu.zjgsu.ito.model;

import java.util.Date;

public class Vedio {
    private Integer pkVedio;

    private String vedioUrl;

    private Date vedioUploadTime;

    private String vedioTitle;

    private Integer fkMcol;

    private Integer fkScol;
    
    private String vedioIntro;

    public Integer getPkVedio() {
        return pkVedio;
    }

    public void setPkVedio(Integer pkVedio) {
        this.pkVedio = pkVedio;
    }

    public String getVedioUrl() {
        return vedioUrl;
    }

    public void setVedioUrl(String vedioUrl) {
        this.vedioUrl = vedioUrl == null ? null : vedioUrl.trim();
    }

    public Date getVedioUploadTime() {
        return vedioUploadTime;
    }

    public void setVedioUploadTime(Date vedioUploadTime) {
        this.vedioUploadTime = vedioUploadTime;
    }

    public String getVedioTitle() {
        return vedioTitle;
    }

    public void setVedioTitle(String vedioTitle) {
        this.vedioTitle = vedioTitle == null ? null : vedioTitle.trim();
    }

    public Integer getFkMcol() {
        return fkMcol;
    }

    public void setFkMcol(Integer fkMcol) {
        this.fkMcol = fkMcol;
    }

    public Integer getFkScol() {
        return fkScol;
    }

    public void setFkScol(Integer fkScol) {
        this.fkScol = fkScol;
    }

	public String getVedioIntro() {
		return vedioIntro;
	}

	public void setVedioIntro(String vedioIntro) {
		this.vedioIntro = vedioIntro;
	}
    
}