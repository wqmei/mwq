package edu.zjgsu.ito.model;

import java.util.Date;

public class Article {
    private Integer pkArticle;

    private String artContentUrl;

    private String artTitle;

    private Date artCreateTime;

    private Integer fkMcol;

    private Integer fkScol;
    
    private String articleContent;

    public Integer getPkArticle() {
        return pkArticle;
    }

    public void setPkArticle(Integer pkArticle) {
        this.pkArticle = pkArticle;
    }

    public String getArtContentUrl() {
        return artContentUrl;
    }

    public void setArtContentUrl(String artContentUrl) {
        this.artContentUrl = artContentUrl == null ? null : artContentUrl.trim();
    }

    public String getArtTitle() {
        return artTitle;
    }

    public void setArtTitle(String artTitle) {
        this.artTitle = artTitle == null ? null : artTitle.trim();
    }

    public Date getArtCreateTime() {
        return artCreateTime;
    }

    public void setArtCreateTime(Date artCreateTime) {
        this.artCreateTime = artCreateTime;
    }

    public Integer getFkMcol() {
        return fkMcol;
    }

    public void setFkMcol(Integer fkMcol) {
        this.fkMcol = fkMcol;
    }

    public Integer getFkScol() {
        return fkScol;
    }

    public void setFkScol(Integer fkScol) {
        this.fkScol = fkScol;
    }

	public String getArticleContent() {
		return articleContent;
	}

	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}
    
    
}