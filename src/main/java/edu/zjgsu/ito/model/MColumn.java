package edu.zjgsu.ito.model;

import java.sql.Timestamp;

public class MColumn {
    private Integer pkMcol;

    private String mcolName;

    private String mcolSum;
    
    private Timestamp addTime;

    public Integer getPkMcol() {
        return pkMcol;
    }

    public void setPkMcol(Integer pkMcol) {
        this.pkMcol = pkMcol;
    }

    public String getMcolName() {
        return mcolName;
    }

    public void setMcolName(String mcolName) {
        this.mcolName = mcolName == null ? null : mcolName.trim();
    }

    public String getMcolSum() {
        return mcolSum;
    }

    public void setMcolSum(String mcolSum) {
        this.mcolSum = mcolSum == null ? null : mcolSum.trim();
    }

	public Timestamp getAddTime() {
		return addTime;
	}

	public void setAddTime(Timestamp addTime) {
		this.addTime = addTime;
	}
    
}