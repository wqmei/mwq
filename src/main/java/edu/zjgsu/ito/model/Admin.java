package edu.zjgsu.ito.model;

import java.sql.Timestamp;
import java.util.Date;

public class Admin {
    private Integer pkAdm;

    private String admName;

    private String admPwd;

    private Timestamp admCreateTime;

    public Integer getPkAdm() {
        return pkAdm;
    }

    public void setPkAdm(Integer pkAdm) {
        this.pkAdm = pkAdm;
    }

    public String getAdmName() {
        return admName;
    }

    public void setAdmName(String admName) {
        this.admName = admName == null ? null : admName.trim();
    }

    public String getAdmPwd() {
        return admPwd;
    }

    public void setAdmPwd(String admPwd) {
        this.admPwd = admPwd == null ? null : admPwd.trim();
    }

    public Timestamp getAdmCreateTime() {
        return admCreateTime;
    }

    public void setAdmCreateTime(Timestamp admCreateTime) {
        this.admCreateTime = admCreateTime;
    }
}