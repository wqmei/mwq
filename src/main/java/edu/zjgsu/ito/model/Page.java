package edu.zjgsu.ito.model;

import java.util.List;

/**
 * 分页对象
 * @author Jenson_Zhou
 *
 * @param <T>
 */
public class Page<T> {

	/**
	 * 实体集合
	 */
	private List<T> entities;
	
	/**
	 * 当前页
	 */
	private Integer curPage;
	
	/**
	 * 每页大小
	 */
	private Integer pageSize;
	
	/**
	 * 总记录数
	 */
	private Integer totalCount;

	public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		this.entities = entities;
	}

	public Integer getCurPage() {
		return curPage;
	}

	public void setCurPage(Integer curPage) {
		this.curPage = curPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
