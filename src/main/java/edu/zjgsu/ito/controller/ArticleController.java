package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.service.IArticleService;
import edu.zjgsu.ito.utils.Constant;

@RestController
public class ArticleController {

	@Autowired
	private IArticleService articleService;

	/**
	 * 查询所有信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "articles", method = RequestMethod.GET)
	public Map<String, Object> findArticles() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Article> article = articleService.getArticles();
		result.put("code", Constant.OK);
		result.put("articles", JSON.toJSON(article));
		return result;
	}

	/**
	 * 以id查询信息
	 * 
	 * @param pkArticle
	 * @return
	 */
	@RequestMapping(value = "article/{pkArticle}", method = RequestMethod.GET)
	public Map<String, Object> findArticle(@PathVariable int pkArticle) {
		Map<String, Object> result = new HashMap<String, Object>();
		Article article = articleService.getArticle(pkArticle);
		result.put("code", Constant.OK);
		result.put("article", JSON.toJSON(article));
		return result;
	}

	/**
	 * 新增信息
	 * 
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "article/add", method = RequestMethod.POST)
	public Map<String, Object> addArticle(@RequestBody Article article) {
		Map<String, Object> result = new HashMap<String, Object>();
		articleService.insertArticle(article);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 根据id删除信息
	 * 
	 * @param pkArticle
	 * @return
	 */
	@RequestMapping(value = "article/del/{pkArticle}", method = RequestMethod.GET)
	public Map<String, Object> delArticle(@PathVariable int pkArticle) {
		Map<String, Object> result = new HashMap<String, Object>();
		articleService.delete(pkArticle);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 修改信息
	 * 
	 * @param article
	 * @return
	 */
	@RequestMapping(value = "article/update", method = RequestMethod.POST)
	public Map<String, Object> updateArticle(@RequestBody Article article) {
		Map<String, Object> result = new HashMap<String, Object>();
		article.setArtContentUrl(articleService.getArticle(article.getPkArticle()).getArtContentUrl());
		if (article.getArtContentUrl() == null)
			throw new RuntimeException("指定信息不存在");
		articleService.updateArticle(article);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 分页查询信息
	 * 
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "article/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findArticleByPage(@PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Article> articlePage = articleService.getArticleByPage(curPage, pageSize);
		List<Article> articles = articlePage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		pageInfo.put("curPage", articlePage.getCurPage());
		pageInfo.put("pageSize", articlePage.getPageSize());
		pageInfo.put("totalCount", articlePage.getTotalCount());
		result.put("pageInfo", pageInfo);
		result.put("articles", articles);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 根据主栏目分页查询所有信息
	 * 
	 * @param curPage
	 * @param pageSize
	 * @param pkMCol
	 * @return
	 */
	@RequestMapping(value = "article/mcol/{pkMCol}/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findArticlePageByMCol(@PathVariable int curPage, @PathVariable int pageSize,
			@PathVariable int pkMCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Article> articlePage = articleService.getArticlePageByMColumn(pkMCol, curPage, pageSize);
		List<Article> articles = articlePage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		pageInfo.put("curPage", articlePage.getCurPage());
		pageInfo.put("pageSize", articlePage.getPageSize());
		pageInfo.put("totalCount", articlePage.getTotalCount());
		result.put("pageInfo", pageInfo);
		result.put("articles", articles);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 根据子栏目分页查询所有信息
	 * 
	 * @param curPage
	 * @param pageSize
	 * @param pkSCol
	 * @return
	 */
	@RequestMapping(value = "article/scol/{pkSCol}/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findArticlePageBySCol(@PathVariable int curPage, @PathVariable int pageSize,
			@PathVariable int pkSCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Article> articlePage = articleService.getArticleBySColumn(pkSCol, curPage, pageSize);
		List<Article> articles = articlePage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		pageInfo.put("curPage", articlePage.getCurPage());
		pageInfo.put("pageSize", articlePage.getPageSize());
		pageInfo.put("totalCount", articlePage.getTotalCount());
		result.put("pageInfo", pageInfo);
		result.put("articles", articles);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 根据主栏目查询所有信息
	 * 
	 * @param pkMCol
	 * @return
	 */
	@RequestMapping(value = "article/mcol/{pkMCol}", method = RequestMethod.GET)
	public Map<String, Object> findArticleByMCol(@PathVariable int pkMCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Article> article = articleService.getArticleByMCol(pkMCol);
		result.put("code", Constant.OK);
		result.put("article", article);
		return result;
	}
	
	/**
	 * 批量删除信息
	 * @param pkArticle
	 * @return
	 */
	@RequestMapping(value = "articles", method = RequestMethod.DELETE)
	public Map<String, Object> deleteSome(@RequestBody String[] pkArticle) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(String i : pkArticle) {
			articleService.delete(Integer.parseInt(i));
		}
		result.put("code", Constant.OK);
		return result;
	}
}
