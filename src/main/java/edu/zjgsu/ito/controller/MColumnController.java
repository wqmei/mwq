package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.MColumn;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.service.IMColService;
import edu.zjgsu.ito.utils.Constant;

@RestController
public class MColumnController {
	@Autowired
	private IMColService mColService;
	
	/**
	 * 新增主栏目
	 * @param mCol
	 * @return
	 */
	@RequestMapping(value = "mcol", method = RequestMethod.POST)
	public Map<String, Object> addMCol(@RequestBody MColumn mCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		mColService.insertMColumn(mCol);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据id查询主栏目
	 * @param pkMCol
	 * @return
	 */
	@RequestMapping(value = "mcol/{pkMCol}", method = RequestMethod.GET)
	public Map<String, Object> findMCol(@PathVariable int pkMCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		MColumn mCol = mColService.getMColumn(pkMCol);
		result.put("mCol", JSON.toJSON(mCol));
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 分页查询所有主栏目
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "mcol/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findMColPage(@PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<MColumn> mColPage = mColService.getMColumnByPage(curPage, pageSize);
		List<MColumn> mCols = mColPage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		pageInfo.put("curPage", mColPage.getCurPage());
		pageInfo.put("pageSize", mColPage.getPageSize());
		pageInfo.put("totalCount", mColPage.getTotalCount());
		result.put("mCols", JSON.toJSON(mCols));
		result.put("pageInfo", JSON.toJSON(pageInfo));
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 删除主栏目
	 * @param pkMCol
	 * @return
	 */
	@RequestMapping(value = "mcol/{pkMCol}", method = RequestMethod.DELETE)
	public Map<String, Object> delMCol(@PathVariable int pkMCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		mColService.deleteMColumn(pkMCol);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 查询所有主栏目
	 * @return
	 */
	@RequestMapping(value = "mcol", method = RequestMethod.GET)
	public Map<String, Object> findMCols() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<MColumn> mCols = mColService.getMColumns();
		result.put("code", Constant.OK);
		result.put("mCols", JSON .toJSON(mCols));
		return result;
	}
	
	/**
	 * 修改主栏目
	 * @return
	 */
	@RequestMapping(value = "mcol", method = RequestMethod.PUT)
	public Map<String, Object> updateMCol(@RequestBody MColumn mcol) {
		Map<String, Object> result = new HashMap<String, Object>();
		mColService.updateMColumn(mcol);
		result.put("code", Constant.OK);
		return result;
	}
}