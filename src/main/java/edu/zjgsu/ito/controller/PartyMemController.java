package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.PartyMember;
import edu.zjgsu.ito.service.IPartyMemService;
import edu.zjgsu.ito.utils.Constant;

@RestController
public class PartyMemController {

	@Autowired
	IPartyMemService partyService;
	
	/**
	 * 新增党员
	 * @param partyMem
	 * @return
	 */
	@RequestMapping(value = "partyMem", method = RequestMethod.POST)
	public Map<String, Object> addPartyMem(@RequestBody PartyMember partyMem) {
		Map<String, Object> result = new HashMap<String, Object>();
		partyService.insertPartyMem(partyMem);
		result.put("code", Constant.OK);
		return result;
		
	}
	
	/**
	 * 根据id查询党员
	 * @param pkPartyMem
	 * @return
	 */
	@RequestMapping(value = "partyMem/{pkPartyMem}", method = RequestMethod.GET)
	public Map<String, Object> findPartyMem(@PathVariable int pkPartyMem) {
		Map<String, Object> result = new HashMap<String, Object>();
		PartyMember partyMem = partyService.getPartyMem(pkPartyMem);
		result.put("code", Constant.OK);
		result.put("partyMem", JSON.toJSON(partyMem));
		return result;
	}
	
	/**
	 * 根据id删除党员
	 * @param pkPartyMem
	 * @return
	 */
	@RequestMapping(value = "partyMem/{pkPartyMem}", method = RequestMethod.DELETE)
	public Map<String, Object> delPartyMem(@PathVariable int pkPartyMem) {
		Map<String, Object> result = new HashMap<String, Object>();
		partyService.deletePartyMem(pkPartyMem);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 修改党员信息
	 * @param partyMem
	 * @return
	 */
	@RequestMapping(value = "partyMem", method = RequestMethod.PUT)
	public Map<String, Object> updatePartyMem(@RequestBody PartyMember partyMem) {
		Map<String, Object> result = new HashMap<String, Object>();
		partyService.updatePartyMem(partyMem);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 分页查询所有党员
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "partyMem/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findPartyMemByPage(@PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<PartyMember> partyMemPage = partyService.getPartyMemByPage(curPage, pageSize);
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		List<PartyMember> partyMems = partyMemPage.getEntities();
		pageInfo.put("curPage", partyMemPage.getCurPage());
		pageInfo.put("pageSize", partyMemPage.getPageSize());
		pageInfo.put("totalCount", partyMemPage.getTotalCount());
		result.put("partyMems", partyMems);
		result.put("pageInfo", pageInfo);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 随机查询5名党员
	 * @return
	 */
	@RequestMapping(value = "partyMem/random", method = RequestMethod.GET)
	public Map<String, Object> find5PartyMemRandomly() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<PartyMember> pms = partyService.get5PartyMemRandomly();
		result.put("code", Constant.OK);
		result.put("partyMem", pms);
		return result;
	}
	
	/**
	 * 批量删除党员
	 * @param pkArticle
	 * @return
	 */
	@RequestMapping(value = "partyMems", method = RequestMethod.DELETE)
	public Map<String, Object> deleteSome(@RequestBody int[] pkPartyMem) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(int i : pkPartyMem) {
			partyService.deletePartyMem(i);
		}
		result.put("code", Constant.OK);
		return result;
	}
}
