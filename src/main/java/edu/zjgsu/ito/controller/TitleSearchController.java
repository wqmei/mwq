package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.model.PartyMember;
import edu.zjgsu.ito.model.Vedio;
import edu.zjgsu.ito.service.IArticleService;
import edu.zjgsu.ito.service.IPartyMemService;
import edu.zjgsu.ito.service.IVedioService;
import edu.zjgsu.ito.utils.Constant;
import edu.zjgsu.ito.vo.ArticleSearchVo;
import edu.zjgsu.ito.vo.PartyMemSearchVo;
import edu.zjgsu.ito.vo.VedioSearchVo;

@RestController
public class TitleSearchController {

	@Autowired
	IArticleService articleService;
	
	@Autowired
	IPartyMemService partyMemService;
	
	@Autowired
	IVedioService videoService;
	
	@RequestMapping(value = "title/{content}", method = RequestMethod.GET)
	public Map<String, Object> SearchByTittle(@PathVariable String content) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<ArticleSearchVo> articles = articleService.getArticleByName(content);
		List<PartyMemSearchVo> partyMems = partyMemService.getPartyMemByName(content);
		List<VedioSearchVo> vedios = videoService.getVedioByName(content);
		
		result.put("articles", articles);
		result.put("partyMems", partyMems);
		result.put("vedios", vedios);
		result.put("code", Constant.OK);
		return result;
	}
}
