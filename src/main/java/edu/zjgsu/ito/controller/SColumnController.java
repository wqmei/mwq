package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.MColumn;
import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.SColumn;
import edu.zjgsu.ito.service.ISColService;
import edu.zjgsu.ito.utils.Constant;

@RestController
public class SColumnController {

	@Autowired
	ISColService sColService;
	
	/**
	 * 新增子栏目
	 * @param sCol
	 * @return
	 */
	@RequestMapping(value = "scol", method = RequestMethod.POST)
	public Map<String, Object> addSCol(@RequestBody SColumn sCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		sColService.insertSColumn(sCol);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据id查询子栏目
	 * @param pkSCol
	 * @return
	 */
	@RequestMapping(value = "scol/{pkSCol}", method = RequestMethod.GET)
	public Map<String, Object> findSCol(@PathVariable int pkSCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		SColumn sCol = sColService.getSColumn(pkSCol);
		result.put("code", Constant.OK);
		result.put("sCol", sCol);
		return result;
	}
	
	/**
	 * 根据主栏目分页查询子栏目
	 * @param pkMCol
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "scol/mcol/{pkMCol}/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findSColPageByMCol(@PathVariable int pkMCol, @PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<SColumn> sColPage = sColService.getSColPageByMCol(curPage, pageSize, pkMCol);
		List<SColumn> sCols = sColPage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		pageInfo.put("curPage", sColPage.getCurPage());
		pageInfo.put("pageSize", sColPage.getPageSize());
		pageInfo.put("totalCount", sColPage.getTotalCount());
		result.put("sCols", JSON.toJSON(sCols));
		result.put("pageInfo", JSON.toJSON(pageInfo));
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据id删除子栏目
	 * @param pkSCol
	 * @return
	 */
	@RequestMapping(value = "scol/{pkSCol}", method = RequestMethod.DELETE)
	public Map<String, Object> delSCol(@PathVariable int pkSCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		sColService.delete(pkSCol);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据主栏目查询子栏目
	 * @param pkMCol
	 * @return
	 */
	@RequestMapping(value = "scol/mcol/{pkMCol}", method = RequestMethod.GET)
	public Map<String, Object> findSColByMCol (@PathVariable int pkMCol) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<SColumn> sCols = sColService.getSColByMCol(pkMCol);
		result.put("sCol", sCols);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 修改主栏目
	 * @return
	 */
	@RequestMapping(value = "scol", method = RequestMethod.PUT)
	public Map<String, Object> updateSCol(@RequestBody SColumn scol) {
		Map<String, Object> result = new HashMap<String, Object>();
		sColService.updateSColumn(scol);
		result.put("code", Constant.OK);
		return result;
	}
}
