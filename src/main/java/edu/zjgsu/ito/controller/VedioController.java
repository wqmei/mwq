package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.Vedio;
import edu.zjgsu.ito.service.IVedioService;
import edu.zjgsu.ito.utils.Constant;

@RestController
public class VedioController {

	@Autowired
	private IVedioService vedioService;
	
	/**
	 * 新增视频
	 * @param vedio
	 * @return
	 */
	@RequestMapping(value = "vedio", method = RequestMethod.POST)
	public Map<String, Object> addVedio(@RequestBody Vedio vedio) {
		Map<String, Object> result = new HashMap<String, Object>();
		vedioService.insertVedio(vedio);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据id删除视频
	 * @param pkVedio
	 * @return
	 */
	@RequestMapping(value = "vedio/delete/{pkVedio}", method = RequestMethod.GET)
	public Map<String, Object> deleteVedio(@PathVariable int pkVedio) {
		Map<String, Object> result = new HashMap<String, Object>();
		vedioService.deleteVedio(pkVedio);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据id查询视频
	 * @param pkVedio
	 * @return
	 */
	@RequestMapping(value = "vedio/{pkVedio}", method = RequestMethod.GET)
	public Map<String, Object> findVedio(@PathVariable int pkVedio) {
		Map<String, Object> result = new HashMap<String, Object>();
		Vedio vedio = vedioService.getVedio(pkVedio);
		result.put("vedio", JSON.toJSON(vedio));
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 修改视频
	 * @param vedio
	 * @return
	 */
	@RequestMapping(value = "vedio/update", method = RequestMethod.POST)
	public Map<String, Object> updateVedio(@RequestBody Vedio vedio) {
		Map<String, Object> result = new HashMap<String, Object>();
		vedioService.updateVedio(vedio);
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 根据主栏目分页查询视频
	 * @param pkMCol
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "vedio/mcol/{pkMCol}/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findVedioPageByMCol(@PathVariable int pkMCol, @PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Vedio> vedioPage = vedioService.getVedioPageByMcol(pkMCol, curPage, pageSize);
		result.put("code", Constant.OK);
		List<Vedio> vedios = vedioPage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		
		pageInfo.put("curPage", vedioPage.getCurPage());
		pageInfo.put("pageSize", vedioPage.getPageSize());
		pageInfo.put("totalCount", vedioPage.getTotalCount());
		result.put("pageInfo", JSON.toJSON(pageInfo));
		result.put("vedios", JSON.toJSON(vedios));
		return result;
	}
	
	/**
	 * 分页查询所有视频
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "vedio/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findVedioByPage(@PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Vedio> vedioPage = vedioService.getVedioByPage(curPage, pageSize);
		result.put("code", Constant.OK);
		List<Vedio> vedios = vedioPage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		
		pageInfo.put("curPage", vedioPage.getCurPage());
		pageInfo.put("pageSize", vedioPage.getPageSize());
		pageInfo.put("totalCount", vedioPage.getTotalCount());
		result.put("pageInfo", JSON.toJSON(pageInfo));
		result.put("vedios", JSON.toJSON(vedios));
		return result;
	}
	
	/**
	 * 根据子栏目分页查询视频
	 * @param pkSCol
	 * @param curPage
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "vedio/scol/{pkSCol}/curPage/{curPage}/pageSize/{pageSize}", method = RequestMethod.GET)
	public Map<String, Object> findVedioPageBySCol(@PathVariable int pkSCol, @PathVariable int curPage, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();
		Page<Vedio> vedioPage = vedioService.getVedioPageByScol(pkSCol, curPage, pageSize);
		result.put("code", Constant.OK);
		List<Vedio> vedios = vedioPage.getEntities();
		Map<String, Object> pageInfo = new HashMap<String, Object>();
		
		pageInfo.put("curPage", vedioPage.getCurPage());
		pageInfo.put("pageSize", vedioPage.getPageSize());
		pageInfo.put("totalCount", vedioPage.getTotalCount());
		result.put("pageInfo", JSON.toJSON(pageInfo));
		result.put("vedios", JSON.toJSON(vedios));
		return result;
	}
	
	/**
	 * 批量删除视频
	 * @param pkArticle
	 * @return
	 */
	@RequestMapping(value = "vedios", method = RequestMethod.DELETE)
	public Map<String, Object> deleteSome(@RequestBody int[] pkVedio) {
		Map<String, Object> result = new HashMap<String, Object>();
		for(int i : pkVedio) {
			vedioService.deleteVedio(i);
		}
		result.put("code", Constant.OK);
		return result;
	}
}
