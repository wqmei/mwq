package edu.zjgsu.ito.controller;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.service.IArticleService;
import edu.zjgsu.ito.service.IFileService;
import edu.zjgsu.ito.utils.Constant;
import edu.zjgsu.ito.utils.FileUtil;

@RestController
public class FileController {

	@Autowired
	IArticleService articleService;
	
	@Autowired
	private IFileService fileService;
	
	@RequestMapping(value = "/fileTest", method = RequestMethod.GET)
	public Map<String, Object> saveFile() {
		String articleUrl = FileUtil.getRandFileName(".txt");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FileUtil.writeTxt("这是一个测试文档", articleUrl, Constant.CONTENT_DIR);
			result.put("code", 200);
			return result;
		} catch (FileNotFoundException e) {
			throw new RuntimeException("正文保存失败");
		}
	}
	
	@RequestMapping(value = "/fileTest2", method = RequestMethod.GET)
	public Map<String, Object> readFile() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Article article = articleService.getArticle(2);
		article.setArticleContent(FileUtil.readTxt(article.getArtContentUrl()));
		result.put("code", 200);
		result.put("article", article);
		return result;
	}
	
	/**
	 * 头像上传
	 * @param file 文件
	 * @return
	 */
	@RequestMapping(value = "files/pmPhoto", method = RequestMethod.POST)
	public Map<String, Object> userHeadUpload(@RequestParam(value = "image") MultipartFile image) {
		Map<String, Object> result = new HashMap<String, Object>();
		String userHeadUrl = fileService.uploadHead(image);
		userHeadUrl = "/upload/photo" + userHeadUrl.substring(userHeadUrl.lastIndexOf("/"));
		//System.out.println("===============" + userHeadUrl);
		result.put("code", Constant.OK);
		result.put("userHeadUrl", userHeadUrl);
		return result;
	}
	
	/**
	 * 视频上传
	 * @param file 文件
	 * @return
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "files/video", method = RequestMethod.POST)
	public Map<String, Object> videoUpload(@RequestParam(value = "video") MultipartFile video) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
		String url = fileService.uploadVideo(video);
		url = "/upload/video" + url.substring(url.lastIndexOf("/"));
		//System.out.println("===============" + url);
		result.put("code", Constant.OK);
		result.put("url", url);
		//return result;
		} catch (RuntimeException e) {
			result.put("code", Constant.FAIL);
			result.put("msg", "视频格式不正确，只允许mp4格式");
			//return result;
		} finally {
			return result;
		}
	}
}
