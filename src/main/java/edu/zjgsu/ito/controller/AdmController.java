package edu.zjgsu.ito.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Admin;
import edu.zjgsu.ito.service.IAdmService;
import edu.zjgsu.ito.utils.Constant;
import edu.zjgsu.ito.vo.AdminLoginVo;
import edu.zjgsu.ito.vo.AdminShowVo;

@RestController
public class AdmController {

	@Autowired
	private IAdmService admService;

	/**
	 * 查询所有管理员
	 * 
	 * @return
	 */
	@RequestMapping(value = "admins", method = RequestMethod.GET)
	public Map<String, Object> showAdms() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<AdminShowVo> admVos = admService.getAdms();
		result.put("code", Constant.OK);
		result.put("adms", JSON.toJSON(admVos));
		return result;
	}

	/**
	 * 以id查询管理员
	 * 
	 * @param pkAdm
	 * @return
	 */
	@RequestMapping(value = "admin/{pkAdm}", method = RequestMethod.GET)
	public Map<String, Object> showAdm(@PathVariable int pkAdm) {
		Map<String, Object> result = new HashMap<String, Object>();
		AdminShowVo admVo = admService.getAdm(pkAdm);
		result.put("code", Constant.OK);
		result.put("adm", JSON.toJSON(admVo));
		return result;
	}

	/**
	 * 修改管理员
	 * 
	 * @param admin
	 * @return
	 */
	@RequestMapping(value = "admin/{pkAdm}", method = RequestMethod.POST)
	public Map<String, Object> updateAdm(@RequestBody Admin admin) {
		Map<String, Object> result = new HashMap<String, Object>();
		admService.updateAdm(admin);
		result.put("code", Constant.OK);
		return result;

	}

	/**
	 * 新增管理员
	 * 
	 * @param admin
	 * @return
	 */
	@RequestMapping(value = "admin", method = RequestMethod.POST)
	public Map<String, Object> addAdm(@RequestBody Admin admin) {
		Map<String, Object> result = new HashMap<String, Object>();
		admService.insertAdm(admin);
		result.put("code", Constant.OK);
		return result;
	}

	/**
	 * 管理员登录
	 * @param adm
	 * @return
	 */
	@RequestMapping(value = "admin/login", method = RequestMethod.POST)
	public Map<String, Object> admLogin(@RequestBody AdminLoginVo adm, HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Object> result = new HashMap<String, Object>();
		switch (admService.admLogin(adm)) {
		case Constant.USER_NOT_EXIST:
			result.put("code", Constant.FAIL);
			result.put("message", Constant.USER_NOT_EXIST_MESSAGE);
			break;
		case Constant.LOGIN_SUCCESS:
			HttpSession session = req.getSession();
			session.setAttribute("admin", adm.getAdmName());
			result.put("code", Constant.OK);
			result.put("message", Constant.LOGIN_SUCCESS_MESSAGE);
			break;
		case Constant.WRONG_PASSWORD:
			result.put("code", Constant.FAIL);
			result.put("message", Constant.WRONG_PASSWORD_MESSAGE);
			break;
		default:
			break;
		}
		return result;
	}
	
	/**
	 * 管理员注销
	 * @return
	 */
	@RequestMapping(value = "admin/logout", method = RequestMethod.GET)
	public Map<String, Object> admLogout(/*HttpServletRequest req, HttpServletResponse resp*/) {
		//req.getSession().removeAttribute("admin");
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", Constant.OK);
		return result;
	}
	
	/**
	 * 重置密码
	 * @return
	 */
	@RequestMapping(value = "admin/reset", method = RequestMethod.GET)
	public Map<String, Object> admReset() {
		Map<String, Object> result = new HashMap<String, Object>();
		if(admService.admReset() == true) {
			result.put("message", Constant.RESET_PWD_SUCCESS_MESSAGE);
			result.put("code", Constant.OK);
		}
		else {
			result.put("message", Constant.RESET_PWD_FAIL_MESSAGE);
			result.put("code", Constant.FAIL);
		}
		return result;
	}
}
