package edu.zjgsu.ito.serviceTest;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.Vedio;
import edu.zjgsu.ito.service.IVedioService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml","classpath:spring-mybatis.xml" })
public class TestVedioService {

	private static final Logger LOGGER = Logger
			.getLogger(TestVedioService.class);
	
	@Autowired
	IVedioService vedioService;
	
	@Test
	public void testGetVedio() {
		Vedio vedio = vedioService.getVedio(1);
		LOGGER.info(JSON.toJSON(vedio));
	}
	
	@Test
	public void testGetVedioPageByScol() {
		Page<Vedio> vedios = vedioService.getVedioPageByScol(1, 1, 10);
		LOGGER.info(JSON.toJSON(vedios));
	}
	
	@Test
	public void testInsertVedio() {
		Vedio vedio = new Vedio();
		vedio.setFkScol(1);
		vedio.setFkMcol(1);
		vedio.setVedioTitle("第三个视频");
		vedio.setVedioUrl("www.youku.com");
		vedioService.insertVedio(vedio);
	}
}
