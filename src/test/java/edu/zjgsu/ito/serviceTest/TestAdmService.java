package edu.zjgsu.ito.serviceTest;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Admin;
import edu.zjgsu.ito.service.IAdmService;
import edu.zjgsu.ito.vo.AdminLoginVo;
import edu.zjgsu.ito.vo.AdminShowVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml","classpath:spring-mybatis.xml" })
public class TestAdmService {

	private static final Logger LOGGER = Logger
			.getLogger(TestAdmService.class);
	
	@Autowired
	IAdmService admService;
	
	/*测试获取所有管理员*/
	@Test
	public void testAllAdm() {
		List<AdminShowVo> admVos = admService.getAdms();
		LOGGER.info(JSON.toJSON(admVos));
		
	}
	
	@Test
	public void testUpdateAdm() {
		Admin adm = admService.getAdmDetail(2);
		adm.setAdmName("李四");
		admService.updateAdm(adm);
		LOGGER.info(JSON.toJSON(admService.getAdm(2)));
	}

	@Test
	public void testInsertAdm() {
		Admin adm = new Admin();
		adm.setAdmName("sjj");
		adm.setAdmPwd("sjjjjj");
		admService.insertAdm(adm);
	}
	
	@Test
	public void testAdmLogin() {
		AdminLoginVo adm = new AdminLoginVo();
		adm.setAdmName("ywy1");
		adm.setAdmPwd("123456");
		System.out.println("登录结果：" + admService.admLogin(adm));
	}
	
	@Test
	public void testReset() {
		admService.admReset();
	}
}
