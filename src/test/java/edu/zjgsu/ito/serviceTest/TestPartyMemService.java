package edu.zjgsu.ito.serviceTest;

import java.sql.Date;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Page;
import edu.zjgsu.ito.model.PartyMember;
import edu.zjgsu.ito.service.IPartyMemService;
import edu.zjgsu.ito.utils.FileUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml","classpath:spring-mybatis.xml" })
public class TestPartyMemService {

	private static final Logger LOGGER = Logger
			.getLogger(TestVedioService.class);
	
	@Autowired
	IPartyMemService partyMemService;
	
	@Test
	public void testGetPartyMemByPage() {
		Page<PartyMember> partyMembers = partyMemService.getPartyMemByPage(1, 10);
		LOGGER.info(JSON.toJSON(partyMembers));
	}
	
	@Test
	public void testAddPartyMem() {
		PartyMember partyMem = new PartyMember();
		Date date = new Date(System.currentTimeMillis());
		partyMem.setPmBirthDate(date);
		partyMem.setPmGender(true);
		partyMem.setPmIntro("我是一个党员");
		partyMem.setPmJob("无业游民");
		partyMem.setPmJoinDate(date);
		partyMem.setPmName("yyy");
		partyMem.setPmPhotoUrl("www.baidu.com");
		partyMemService.insertPartyMem(partyMem);
	}
	
	@Test
	public void testFind5Randomly() {
		LOGGER.info(JSON.toJSON(partyMemService.get5PartyMemRandomly()));
	}
	
	@Test
	public void testSaveFile() {
		FileUtil.savePhoto("C:\\Users\\sjjok\\Pictures\\Saved Pictures\\bird.jpg");
	}
}
