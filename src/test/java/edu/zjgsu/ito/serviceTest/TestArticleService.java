package edu.zjgsu.ito.serviceTest;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;

import edu.zjgsu.ito.model.Article;
import edu.zjgsu.ito.service.IArticleService;
import edu.zjgsu.ito.vo.ArticleSearchVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml","classpath:spring-mybatis.xml" })
public class TestArticleService {

	private static final Logger LOGGER = Logger
			.getLogger(TestAdmService.class);
	
	@Autowired
	IArticleService articleService;

	@Test
	public void testFindArticleByName() {
		List<ArticleSearchVo> articles = articleService.getArticleByName("十七");
		LOGGER.info(JSON.toJSON(articles));
	}
	
	@Test
	public void test() {
		int[] a = {1,2,3,4,5};
		System.out.println(JSON.toJSON(a));
	}
}
