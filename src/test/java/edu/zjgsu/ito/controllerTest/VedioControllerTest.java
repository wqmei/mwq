package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

public class VedioControllerTest extends BaseControllerTest {

	@Test
	public void testAddVedio() {
		String content = "{\"fkScol\":1,\"vedioUrl\":\"www.baidu.com\",\"vedioTitle\":\"第一个视频\",\"fkMcol\":1}";
		doPost("/vedio",content);
	}
	
	@Test
	public void testFindVedioByPage() {
		doGet("/vedio/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindVedioPageByMCol() {
		doGet("/vedio/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindVedioPageBySCol() {
		doGet("/vedio/scol/2/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindVedio() {
		doGet("/vedio/1");
	}
}
