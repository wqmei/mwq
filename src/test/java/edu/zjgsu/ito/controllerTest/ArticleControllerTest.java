package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

import edu.zjgsu.ito.utils.Constant;

public class ArticleControllerTest extends BaseControllerTest{

	@Test
	public void testFindArticles() {
		doGet("/articles");
	}

	@Test
	public void testFindArticle() {
		doGet("/article/6");
	}
	
	@Test
	public void testAddArticle() {
		String content = "{\"articleContent\":\"测试controller新增的文档\",\"fkScol\":1,\"artTitle\":\"asidj\",\"fkMcol\":1}";
		doPost("/article/add", content);
	}
	
	@Test
	public void testDelArticle() {
		doGet("/article/del/3"); 
	}
	
	@Test
	public void testUpdateArticle() {
		String content = "{\"pkArticle\":2,\"articleContent\":\"这是修改后的版本\",\"fkScol\":1,\"artTitle\":\"asidj\",\"fkMcol\":1}";
		doPost("/article/update", content);
	}
	
	@Test
	public void testFindArticleByPage() {
		doGet("/article/curPage/2/pageSize/2");
	}
	
	@Test
	public void testFindArticlePageByMCol() {
		doGet("/article/mcol/1/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindArticlePageBySCol() {
		doGet("/article/scol/2/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindArticleByMCol() {
		doGet("/article/mcol/2");
	}
	
	@Test
	public void test() {
		System.out.println(Constant.CONTENT_DIR);
	}
}
