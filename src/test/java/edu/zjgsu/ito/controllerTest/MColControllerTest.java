package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

public class MColControllerTest extends BaseControllerTest {

	@Test
	public void testAddMCol() {
		String content = "{\"mcolSum\":\"这是第不知道几个主栏目\",\"mcolName\":\"主栏目N\"}";
		doPost("/mcol", content);
	}

	@Test
	public void testFindMCol() {
		doGet("/mcol/1");
	}
	
	@Test 
	public void testFindMColByPage() {
		doGet("/mcol/curPage/1/pageSize/10");
	}
	
	@Test
	public void testDelMCol() {
		doDelete("/mcol/2");
	}
	
	@Test
	public void testFindMCols() {
		doGet("/mcol");
	}
	
	@Test
	public void testUpdate() {
		String content = "{\"pkMcol\":1,\"mcolSum\":\"这是第机器化个主栏目\",\"mcolName\":\"主栏目一\"}";
		doPut("/mcol", content);
	}
}
