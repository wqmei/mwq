package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

public class PartyMemControllerTest extends BaseControllerTest {

	@Test
	public void testAddPartyMem() {
		String content = "{\"pmPhotoUrl\":\"asds\",\"pmBirthDate\":1476547200000,\"pmGender\":true,\"pmName\":\"sss\",\"pmJob\":\"无\",\"pmIntro\":\"我是一个好人\",\"pmJoinDate\":1476547200000}";
		doPost("/partyMem", content);
	}
	
	@Test
	public void testFindPartyMem() {
		doGet("/partyMem/1");
	}
	
	@Test
	public void testDelPartyMem() {
		doDelete("/partyMem/3");
	}
	
	@Test
	public void testUpdateMem() {
		String photoUrl = "C:/Users/sjjok/Pictures/Saved Pictures/ab.jpg";
		String content = "{\"pmPhotoUrl\":\""+photoUrl+"\",\"pmBirthDate\":1476547200000,\"pmGender\":true,\"pmName\":\"aa4a\",\"pkPm\":2,\"pmJob\":\"无\",\"pmIntro\":\"我是一个好人\",\"pmJoinDate\":1476547200000}";
		System.out.println("==============================" + content);
		doPut("/partyMem", content);
	}
	
	@Test
	public void testFindPartyMemByPage() {
		doGet("/partyMem/curPage/1/pageSize/10");
	}
	
	@Test
	public void testFindRandomly() {
		doGet("/partyMem/random");
	}
}
