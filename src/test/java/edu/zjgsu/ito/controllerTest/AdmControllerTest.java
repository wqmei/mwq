package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

public class AdmControllerTest extends BaseControllerTest {
	
	@Test
	public void testFindadms() {
		doGet("/admins");
	}
	
	@Test
	public void testFindAdm() {
		doGet("/admin/2");
	}

	@Test
	public void testUpdateAdm() {
		String content = "{\"admPwd\":\"28877110\",\"admName\":\"admin\",\"pkAdm\":1}";
		doPost("/admin/1", content);
	}
	
	@Test
	public void testAdmLogin() {
		String content = "{\"admPwd\":\"28877110\",\"admName\":\"admin\"}";
		doPost("/admin/login",content);
	}
	
	@Test
	public void testReset() {
		doGet("/admin/reset");
	}
}
