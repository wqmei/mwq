package edu.zjgsu.ito.controllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSON;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")
@ContextConfiguration({ "classpath:spring.xml","classpath:spring-mybatis.xml","classpath:spring-mvc.xml"})
public abstract class BaseControllerTest {
	
	@Autowired
	private WebApplicationContext wac;

	protected MockMvc mockMvc;
	protected String result;

	@Before
	public void setup() throws Exception {

		mockMvc = webAppContextSetup(this.wac).build();

//		mockMvc = webAppContextSetup(this.wac).build();

	}
	
	@After
	public void tearDown() {
		assertCode(result);
	}
	
	/**
	 * 带参post请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doPost(String url, String content) {
		try {
			MvcResult mr = mockMvc.perform(post(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON).content(content)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 不带参post请求
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public String doPost(String url){
		try {
			MvcResult mr = mockMvc.perform(post(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 带参get请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doGet(String url, String content) {
		try {
			MvcResult mr = mockMvc.perform(get(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON).content(content)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 不带参get请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doGet(String url) {
		try {
			MvcResult mr = mockMvc.perform(get(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 带参put请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doPut(String url, String content) {
		try {
			MvcResult mr = mockMvc.perform(put(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON).content(content)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 不带参put请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doPut(String url) {
		try {
			MvcResult mr = mockMvc.perform(put(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 带参delete请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doDelete(String url, String content) {
		try {
			MvcResult mr = mockMvc.perform(delete(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON).content(content)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 不带参delete请求
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String doDelete(String url) {
		try {
			MvcResult mr = mockMvc.perform(delete(url).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON)).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()).andReturn();
			result = mr.getResponse().getContentAsString();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		return result;
	}
	
	/**
	 * 断言状态码
	 * @param result
	 */
	@SuppressWarnings("rawtypes")
	public void assertCode(String result) {
		Map map = null;
		
		try{
			map = JSON.parseObject(result, Map.class);
		}catch(Exception e) {
			fail("返回数据不是JSON类型");
		}
		if(map == null || map.isEmpty()) {
			fail("junit没有接收到返回数据");
		}
		if(!map.containsKey("code")){
			fail("缺少返回码code");
		}
		Integer code = (Integer) map.get("code");
		assertEquals("返回码code不为200", (Integer)200, code);
	}
}