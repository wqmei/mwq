package edu.zjgsu.ito.controllerTest;

import java.io.File;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class FileControllerTest extends BaseControllerTest {

	@Test
	public void testSaveFile() {
		doGet("/fileTest");
	}

	@Test
	public void testReadFile() {
		doGet("/fileTest2");
	}
	
	/**
	 * 测试获取用户路径
	 */
	@Test
	public void testGetUserDir() {
		System.out.println(System.getProperty("user.dir"));
	}
	
	@Test
	public void test() {
		String path = "D:/apache-tomcat-7.0.67/upload/head/";
		
		String dir = "D:/apache-tomcat-7.0.67/";
		System.out.println(path.substring(dir.length()));
	}
	
}
