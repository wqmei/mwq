package edu.zjgsu.ito.controllerTest;

import org.junit.Test;

public class SColControllerTest extends BaseControllerTest {

	@Test
	public void testFindSCol() {
		doGet("/scol/1");
	}

	@Test
	public void testFindSColPageByMCol() {
		doGet("/scol/mcol/1/curPage/1/pageSize/2");
	}
	
	@Test
	public void testDelSCol() {
		doDelete("/scol/2");
	}
	
	@Test
	public void testAddSCol() {
		String content = "{\"scolName\":\"子栏目二\",\"scolSum\":\"这是第二个子栏目\",\"fkMcol\":2}";
		doPost("/scol", content);
	}
	
	@Test
	public void testFindSColByMCol() {
		doGet("/scol/mcol/1");
	}
	
	@Test
	public void testUpdate() {
		String content = "{\"pkScol\":1,\"scolName\":\"子栏目黄日华\",\"scolSum\":\"这是奇偶诶嘿个子栏目\",\"fkMcol\":1}";
		doPut("/scol", content);
	}
}
