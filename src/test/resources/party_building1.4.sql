/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : party_building

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2016-11-10 20:39:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `pk_adm` int(10) NOT NULL AUTO_INCREMENT,
  `adm_name` varchar(63) NOT NULL,
  `adm_pwd` varchar(127) NOT NULL,
  `adm_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_adm`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2016-10-20 12:58:46');

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article` (
  `pk_article` int(10) NOT NULL AUTO_INCREMENT COMMENT '标题',
  `art_content_url` varchar(127) NOT NULL COMMENT '内容存放路径',
  `art_title` varchar(127) NOT NULL COMMENT '标题',
  `art_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目id',
  `fk_scol` int(10) DEFAULT NULL COMMENT '所属子栏目id',
  PRIMARY KEY (`pk_article`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_article_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_article_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_article
-- ----------------------------
INSERT INTO `tb_article` VALUES ('30', '201611100557590661340.txt', '信息学院创先争优特色项目——全员全程导师制的建设与实践', '2016-11-10 17:57:59', '1', '2');
INSERT INTO `tb_article` VALUES ('31', '201611100610568185941.txt', '大', '2016-11-10 18:10:57', '2', '4');
INSERT INTO `tb_article` VALUES ('32', '201611100612435581745.txt', '呵呵', '2016-11-10 18:12:44', '2', '4');
INSERT INTO `tb_article` VALUES ('33', '201611100636570599529.txt', '及价格', '2016-11-10 18:36:57', '2', '4');
INSERT INTO `tb_article` VALUES ('34', '201611100637117228724.txt', '百次', '2016-11-10 18:37:12', '5', '11');
INSERT INTO `tb_article` VALUES ('35', '201611100637380354344.txt', '撒', '2016-11-10 18:37:38', '1', '2');
INSERT INTO `tb_article` VALUES ('36', '201611100637497302561.txt', '撒', '2016-11-10 18:37:50', '3', '7');
INSERT INTO `tb_article` VALUES ('37', '201611100638021786773.txt', '你', '2016-11-10 18:38:02', '2', '5');
INSERT INTO `tb_article` VALUES ('38', '201611100638107356918.txt', '烦', '2016-11-10 18:38:11', '2', '5');
INSERT INTO `tb_article` VALUES ('39', '201611100638139833796.txt', '烦', '2016-11-10 18:38:14', '2', '5');
INSERT INTO `tb_article` VALUES ('40', '201611100638159479218.txt', '烦', '2016-11-10 18:38:16', '2', '5');
INSERT INTO `tb_article` VALUES ('41', '201611100638593622077.txt', '飞', '2016-11-10 18:38:59', '3', '8');

-- ----------------------------
-- Table structure for tb_mcol
-- ----------------------------
DROP TABLE IF EXISTS `tb_mcol`;
CREATE TABLE `tb_mcol` (
  `pk_mcol` int(10) NOT NULL AUTO_INCREMENT COMMENT '主栏目id',
  `mcol_name` varchar(127) NOT NULL COMMENT '主栏目名称',
  `mcol_sum` varchar(255) DEFAULT NULL COMMENT '主栏目概述',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_mcol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_mcol
-- ----------------------------
INSERT INTO `tb_mcol` VALUES ('1', '党建动态', '', '2016-10-20 12:13:01');
INSERT INTO `tb_mcol` VALUES ('2', '创先争优', '', '2016-10-20 12:13:13');
INSERT INTO `tb_mcol` VALUES ('3', '网上党校', null, '2016-10-20 12:13:24');
INSERT INTO `tb_mcol` VALUES ('4', '党员风采', null, '2016-10-20 12:13:38');
INSERT INTO `tb_mcol` VALUES ('5', '党建视频', null, '2016-10-20 12:13:48');
INSERT INTO `tb_mcol` VALUES ('6', '党建系统', null, '2016-10-20 12:13:57');

-- ----------------------------
-- Table structure for tb_pm
-- ----------------------------
DROP TABLE IF EXISTS `tb_pm`;
CREATE TABLE `tb_pm` (
  `pk_pm` int(10) NOT NULL AUTO_INCREMENT COMMENT '党员id',
  `pm_name` varchar(63) NOT NULL COMMENT '党员姓名',
  `pm_job` varchar(127) NOT NULL COMMENT '党员工作',
  `pm_birth_date` date NOT NULL COMMENT '出生年月',
  `pm_join_date` date NOT NULL COMMENT '入党日期',
  `pm_intro` varchar(1000) NOT NULL COMMENT '党员介绍',
  `pm_gender` tinyint(1) NOT NULL COMMENT '党员性别，1为男，2为女',
  `pm_photo_url` varchar(127) NOT NULL COMMENT '照片路径',
  PRIMARY KEY (`pk_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pm
-- ----------------------------
INSERT INTO `tb_pm` VALUES ('4', '张三', '无', '2016-10-21', '2016-10-11', '<p>无<br/></p>', '1', '../../img/201610280846033803838465135.jpg');
INSERT INTO `tb_pm` VALUES ('9', 'sjj', 'none', '2009-11-09', '2016-11-17', '<p>Giuseppe</p>', '1', '/upload/photo/201611100907135552360.jpg');

-- ----------------------------
-- Table structure for tb_scol
-- ----------------------------
DROP TABLE IF EXISTS `tb_scol`;
CREATE TABLE `tb_scol` (
  `pk_scol` int(10) NOT NULL AUTO_INCREMENT COMMENT '子栏目id',
  `scol_name` varchar(127) NOT NULL COMMENT '子栏目名称',
  `scol_sum` varchar(255) DEFAULT NULL COMMENT '子栏目概述',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_scol`),
  KEY `fk_mcol` (`fk_mcol`),
  CONSTRAINT `tb_scol_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_scol
-- ----------------------------
INSERT INTO `tb_scol` VALUES ('1', '公告公示', null, '1', '2016-10-20 12:16:20');
INSERT INTO `tb_scol` VALUES ('2', '党建新闻', null, '1', '2016-10-20 12:16:39');
INSERT INTO `tb_scol` VALUES ('4', '理论学习', null, '2', '2016-10-20 12:17:49');
INSERT INTO `tb_scol` VALUES ('5', '活动动态', null, '2', '2016-10-20 12:17:22');
INSERT INTO `tb_scol` VALUES ('6', '特色项目', null, '2', '2016-10-20 12:17:37');
INSERT INTO `tb_scol` VALUES ('7', '模拟试题', null, '3', '2016-10-20 12:17:53');
INSERT INTO `tb_scol` VALUES ('8', '党建制作', null, '3', '2016-10-20 12:18:19');
INSERT INTO `tb_scol` VALUES ('9', '文献著作', null, '3', '2016-10-20 12:18:26');
INSERT INTO `tb_scol` VALUES ('10', '党章', null, '3', '2016-10-20 12:18:47');
INSERT INTO `tb_scol` VALUES ('11', '专题报告', null, '5', '2016-10-20 12:19:05');
INSERT INTO `tb_scol` VALUES ('12', '党的历史', null, '5', '2016-10-20 12:19:16');
INSERT INTO `tb_scol` VALUES ('13', '民主评议', null, '6', '2016-10-20 12:19:37');
INSERT INTO `tb_scol` VALUES ('14', '撒', '撒', '3', '2016-11-10 18:40:51');
INSERT INTO `tb_scol` VALUES ('15', '撒', '撒', '1', '2016-11-10 18:41:12');
INSERT INTO `tb_scol` VALUES ('16', '可怜', '可怜', '4', '2016-11-10 18:41:35');

-- ----------------------------
-- Table structure for tb_vedio
-- ----------------------------
DROP TABLE IF EXISTS `tb_vedio`;
CREATE TABLE `tb_vedio` (
  `pk_vedio` int(10) NOT NULL AUTO_INCREMENT COMMENT '视频id',
  `vedio_url` varchar(127) NOT NULL COMMENT '视频存放位置',
  `vedio_upload_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '视频上传时间',
  `vedio_title` varchar(127) NOT NULL,
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `fk_scol` int(10) DEFAULT NULL COMMENT '所属子栏目',
  `vedio_intro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_vedio`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_vedio_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_vedio_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_vedio
-- ----------------------------
INSERT INTO `tb_vedio` VALUES ('4', '<p>/upload/video/201611100327326227428.mp4大</p><p><br/></p><p><br/></p><p><br/></p><p>发的飒飒</p><p><br/></p>', '2016-11-10 19:05:49', '核桃仁', '2', '5', '热接听呢比博尔特呢');
INSERT INTO `tb_vedio` VALUES ('5', '/upload/video/201611100434554837180.rmvb', '2016-11-10 16:35:10', 'rmvb视频测试', '1', '1', 'rmvb');
INSERT INTO `tb_vedio` VALUES ('6', '/upload/video/201611100438265937199.wmv', '2016-11-10 16:38:41', 'wmv测试', '1', '2', 'wmv');
INSERT INTO `tb_vedio` VALUES ('8', '/upload/video/201611100652137912763.mp4', '2016-11-10 18:52:17', '视频', '1', '2', '视频');
INSERT INTO `tb_vedio` VALUES ('9', '/upload/video/201611100653324374635.wmv', '2016-11-10 18:53:33', '小书品', '2', '4', '撒');
INSERT INTO `tb_vedio` VALUES ('10', '/upload/video/201611100653324374635.wmv', '2016-11-10 18:53:38', '小书品', '2', '4', '撒');
