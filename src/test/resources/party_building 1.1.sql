/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : party_building

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2016-10-19 18:41:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `pk_adm` int(10) NOT NULL AUTO_INCREMENT,
  `adm_name` varchar(63) NOT NULL,
  `adm_pwd` varchar(127) NOT NULL,
  `adm_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_adm`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', '00', '123456', '2016-10-13 09:37:25');
INSERT INTO `tb_admin` VALUES ('2', 'ywy', 'e10adc3949ba59abbe56e057f20f883e', '2016-09-26 16:10:09');

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article` (
  `pk_article` int(10) NOT NULL AUTO_INCREMENT COMMENT '标题',
  `art_content_url` varchar(127) NOT NULL COMMENT '内容存放路径',
  `art_title` varchar(127) NOT NULL COMMENT '标题',
  `art_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目id',
  `fk_scol` int(10) NOT NULL COMMENT '所属子栏目id',
  PRIMARY KEY (`pk_article`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_article_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_article_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_article
-- ----------------------------
INSERT INTO `tb_article` VALUES ('2', '201610130414039636040.txt', 'asidj', '2016-10-13 16:27:16', '1', '1');

-- ----------------------------
-- Table structure for tb_mcol
-- ----------------------------
DROP TABLE IF EXISTS `tb_mcol`;
CREATE TABLE `tb_mcol` (
  `pk_mcol` int(10) NOT NULL AUTO_INCREMENT COMMENT '主栏目id',
  `mcol_name` varchar(127) NOT NULL COMMENT '主栏目名称',
  `mcol_sum` varchar(255) DEFAULT NULL COMMENT '主栏目概述',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_mcol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_mcol
-- ----------------------------
INSERT INTO `tb_mcol` VALUES ('1', '主栏目一', '这是第一个主栏目', '2016-10-19 18:23:09');
INSERT INTO `tb_mcol` VALUES ('2', '主栏目N', '这是第不知道几个主栏目', '2016-10-19 18:36:05');

-- ----------------------------
-- Table structure for tb_pm
-- ----------------------------
DROP TABLE IF EXISTS `tb_pm`;
CREATE TABLE `tb_pm` (
  `pk_pm` int(10) NOT NULL AUTO_INCREMENT COMMENT '党员id',
  `pm_name` varchar(63) NOT NULL COMMENT '党员姓名',
  `pm_job` varchar(127) NOT NULL COMMENT '党员工作',
  `pm_birth_date` date NOT NULL COMMENT '出生年月',
  `pm_join_date` date NOT NULL COMMENT '入党日期',
  `pm_intro` varchar(1000) NOT NULL COMMENT '党员介绍',
  `pm_gender` tinyint(1) NOT NULL COMMENT '党员性别，1为男，2为女',
  `pm_photo_url` varchar(127) NOT NULL COMMENT '照片路径',
  PRIMARY KEY (`pk_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pm
-- ----------------------------
INSERT INTO `tb_pm` VALUES ('1', 'yyy', '无业游民', '2016-10-08', '2016-10-08', '我是一个党员', '1', 'www.baidu.com');
INSERT INTO `tb_pm` VALUES ('2', 'aaa', '无', '2016-10-16', '2016-10-16', '我是一个好人', '1', 'asds');
INSERT INTO `tb_pm` VALUES ('3', '1212', 'qwe', '2016-10-05', '2016-10-19', 'qwew', '1', 'qweqewe');

-- ----------------------------
-- Table structure for tb_scol
-- ----------------------------
DROP TABLE IF EXISTS `tb_scol`;
CREATE TABLE `tb_scol` (
  `pk_scol` int(10) NOT NULL AUTO_INCREMENT COMMENT '子栏目id',
  `scol_name` varchar(127) NOT NULL COMMENT '子栏目名称',
  `scol_sum` varchar(255) DEFAULT NULL COMMENT '子栏目概述',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_scol`),
  KEY `fk_mcol` (`fk_mcol`),
  CONSTRAINT `tb_scol_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_scol
-- ----------------------------
INSERT INTO `tb_scol` VALUES ('1', '子栏目一', '这是一个子栏目', '1', '2016-10-19 18:23:17');
INSERT INTO `tb_scol` VALUES ('2', '子栏目二', '这是第二个子栏目', '2', '2016-10-19 18:39:08');

-- ----------------------------
-- Table structure for tb_vedio
-- ----------------------------
DROP TABLE IF EXISTS `tb_vedio`;
CREATE TABLE `tb_vedio` (
  `pk_vedio` int(10) NOT NULL AUTO_INCREMENT COMMENT '视频id',
  `vedio_url` varchar(127) NOT NULL COMMENT '视频存放位置',
  `vedio_upload_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '视频上传时间',
  `vedio_title` varchar(127) NOT NULL,
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `fk_scol` int(10) NOT NULL COMMENT '所属子栏目',
  PRIMARY KEY (`pk_vedio`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_vedio_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_vedio_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_vedio
-- ----------------------------
INSERT INTO `tb_vedio` VALUES ('1', 'www.baidu.com', '2016-10-07 13:45:03', '第一个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('2', 'www.iqiyi.com', '2016-10-08 13:45:39', '第二个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('3', 'www.youku.com', '2016-10-14 14:09:54', '第三个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('4', 'www.youku.com', '2016-10-13 15:03:45', '第三个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('5', 'www.youku.com', '2016-10-08 14:06:10', '第三个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('6', 'www.youku.com', '2016-10-08 14:09:46', '第三个视频', '1', '1');
INSERT INTO `tb_vedio` VALUES ('7', 'www.baidu.com', '2016-10-17 08:55:50', '第一个视频', '1', '1');
