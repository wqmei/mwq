/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : party_building

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2016-10-20 12:22:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `pk_adm` int(10) NOT NULL AUTO_INCREMENT,
  `adm_name` varchar(63) NOT NULL,
  `adm_pwd` varchar(127) NOT NULL,
  `adm_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_adm`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2016-10-20 12:11:43');

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article` (
  `pk_article` int(10) NOT NULL AUTO_INCREMENT COMMENT '标题',
  `art_content_url` varchar(127) NOT NULL COMMENT '内容存放路径',
  `art_title` varchar(127) NOT NULL COMMENT '标题',
  `art_create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目id',
  `fk_scol` int(10) DEFAULT NULL COMMENT '所属子栏目id',
  PRIMARY KEY (`pk_article`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_article_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_article_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_article
-- ----------------------------

-- ----------------------------
-- Table structure for tb_mcol
-- ----------------------------
DROP TABLE IF EXISTS `tb_mcol`;
CREATE TABLE `tb_mcol` (
  `pk_mcol` int(10) NOT NULL AUTO_INCREMENT COMMENT '主栏目id',
  `mcol_name` varchar(127) NOT NULL COMMENT '主栏目名称',
  `mcol_sum` varchar(255) DEFAULT NULL COMMENT '主栏目概述',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_mcol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_mcol
-- ----------------------------
INSERT INTO `tb_mcol` VALUES ('1', '党建动态', '', '2016-10-20 12:13:01');
INSERT INTO `tb_mcol` VALUES ('2', '创先争优', '', '2016-10-20 12:13:13');
INSERT INTO `tb_mcol` VALUES ('3', '网上党校', null, '2016-10-20 12:13:24');
INSERT INTO `tb_mcol` VALUES ('4', '党员风采', null, '2016-10-20 12:13:38');
INSERT INTO `tb_mcol` VALUES ('5', '党建视频', null, '2016-10-20 12:13:48');
INSERT INTO `tb_mcol` VALUES ('6', '党建系统', null, '2016-10-20 12:13:57');

-- ----------------------------
-- Table structure for tb_pm
-- ----------------------------
DROP TABLE IF EXISTS `tb_pm`;
CREATE TABLE `tb_pm` (
  `pk_pm` int(10) NOT NULL AUTO_INCREMENT COMMENT '党员id',
  `pm_name` varchar(63) NOT NULL COMMENT '党员姓名',
  `pm_job` varchar(127) NOT NULL COMMENT '党员工作',
  `pm_birth_date` date NOT NULL COMMENT '出生年月',
  `pm_join_date` date NOT NULL COMMENT '入党日期',
  `pm_intro` varchar(1000) NOT NULL COMMENT '党员介绍',
  `pm_gender` tinyint(1) NOT NULL COMMENT '党员性别，1为男，2为女',
  `pm_photo_url` varchar(127) NOT NULL COMMENT '照片路径',
  PRIMARY KEY (`pk_pm`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pm
-- ----------------------------
INSERT INTO `tb_pm` VALUES ('1', 'yyy', '无业游民', '2016-10-08', '2016-10-08', '我是一个党员', '1', 'www.baidu.com');
INSERT INTO `tb_pm` VALUES ('2', 'aaa', '无', '2016-10-16', '2016-10-16', '我是一个好人', '1', 'asds');
INSERT INTO `tb_pm` VALUES ('3', '1212', 'qwe', '2016-10-05', '2016-10-19', 'qwew', '1', 'qweqewe');

-- ----------------------------
-- Table structure for tb_scol
-- ----------------------------
DROP TABLE IF EXISTS `tb_scol`;
CREATE TABLE `tb_scol` (
  `pk_scol` int(10) NOT NULL AUTO_INCREMENT COMMENT '子栏目id',
  `scol_name` varchar(127) NOT NULL COMMENT '子栏目名称',
  `scol_sum` varchar(255) DEFAULT NULL COMMENT '子栏目概述',
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_scol`),
  KEY `fk_mcol` (`fk_mcol`),
  CONSTRAINT `tb_scol_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_scol
-- ----------------------------
INSERT INTO `tb_scol` VALUES ('1', '公告公示', null, '1', '2016-10-20 12:16:20');
INSERT INTO `tb_scol` VALUES ('2', '党建新闻', null, '1', '2016-10-20 12:16:39');
INSERT INTO `tb_scol` VALUES ('4', '理论学习', null, '2', '2016-10-20 12:17:49');
INSERT INTO `tb_scol` VALUES ('5', '活动动态', null, '2', '2016-10-20 12:17:22');
INSERT INTO `tb_scol` VALUES ('6', '特色项目', null, '2', '2016-10-20 12:17:37');
INSERT INTO `tb_scol` VALUES ('7', '模拟试题', null, '3', '2016-10-20 12:17:53');
INSERT INTO `tb_scol` VALUES ('8', '党建制作', null, '3', '2016-10-20 12:18:19');
INSERT INTO `tb_scol` VALUES ('9', '文献著作', null, '3', '2016-10-20 12:18:26');
INSERT INTO `tb_scol` VALUES ('10', '党章', null, '3', '2016-10-20 12:18:47');
INSERT INTO `tb_scol` VALUES ('11', '专题报告', null, '5', '2016-10-20 12:19:05');
INSERT INTO `tb_scol` VALUES ('12', '党的历史', null, '5', '2016-10-20 12:19:16');
INSERT INTO `tb_scol` VALUES ('13', '民主评议', null, '6', '2016-10-20 12:19:37');

-- ----------------------------
-- Table structure for tb_vedio
-- ----------------------------
DROP TABLE IF EXISTS `tb_vedio`;
CREATE TABLE `tb_vedio` (
  `pk_vedio` int(10) NOT NULL AUTO_INCREMENT COMMENT '视频id',
  `vedio_url` varchar(127) NOT NULL COMMENT '视频存放位置',
  `vedio_upload_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '视频上传时间',
  `vedio_title` varchar(127) NOT NULL,
  `fk_mcol` int(10) NOT NULL COMMENT '所属主栏目',
  `fk_scol` int(10) DEFAULT NULL COMMENT '所属子栏目',
  PRIMARY KEY (`pk_vedio`),
  KEY `fk_mcol` (`fk_mcol`),
  KEY `fk_scol` (`fk_scol`),
  CONSTRAINT `tb_vedio_ibfk_1` FOREIGN KEY (`fk_mcol`) REFERENCES `tb_mcol` (`pk_mcol`) ON UPDATE CASCADE,
  CONSTRAINT `tb_vedio_ibfk_2` FOREIGN KEY (`fk_scol`) REFERENCES `tb_scol` (`pk_scol`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_vedio
-- ----------------------------
